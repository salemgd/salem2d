#ifndef SALEM2D_TRANSFORM_HPP
#define SALEM2D_TRANSFORM_HPP

#include "Salem2D/module.h"

namespace Salem2D::Physics {
    class SALEM_API Transform {
    public:
        Transform(float p_x, float p_y, float p_z) : m_x(p_x), m_y(p_y), m_z(p_z) {}

        float x() { return m_x; }
        float y() { return m_y; }
        float z() { return m_z; }

        void set_x(float p_x) { m_x = p_x; }
        void set_y(float p_y) { m_y = p_y; }
        void set_z(float p_z) { m_z = p_z; }
    private:
        float m_x, m_y, m_z;
    };
}
#endif //SALEM2D_TRANSFORM_HPP
