#pragma once

#ifdef _WIN32
#  ifdef SALEM2D_EXPORTS
#    define SALEM_API __declspec(dllexport)
#  else
#    define SALEM_API __declspec(dllimport)
#  endif
#else
#	define SALEM_API
#endif