#ifndef SALEM2D_PACKET_HPP
#define SALEM2D_PACKET_HPP

#include <string>
#include <string.h>
#include <stdio.h>
#include <boost/asio.hpp>

#include "Salem2D/module.h"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network {
    class Packet {
    public:
		SALEM_API Packet(std::string p_event_hook, std::unique_ptr<Events::NetworkObserver>& p_observer, size_t p_bytes_read = 0);
		SALEM_API Packet(const char* p_buf, std::unique_ptr<Events::NetworkObserver>& p_observer, size_t p_bytes_read);

        // TODO: Protect from buffer overloading
		SALEM_API void AddArgument(size_t p_argument_size, const void* p_value)
        {
            PacketArgument argument;
            argument.arg_size = p_argument_size;
            argument.arg_value = malloc(p_argument_size);
            std::memcpy(argument.arg_value, reinterpret_cast<const char*>(p_value), p_argument_size);
            m_arguments.push_back(argument);
        }

		SALEM_API void AddArgument(std::string p_value) {
			AddArgument(p_value.data());
		}

        SALEM_API void AddArgument(const char* p_value) {
            auto data = std::string(p_value) + '\0';
            AddArgument(data.length(), reinterpret_cast<const void*>(data.data()));
        }

		SALEM_API const char* generate_buffer();
		SALEM_API std::string get_event_hook();
		SALEM_API int get_event_hook_index();
		SALEM_API size_t get_packet_size();

		template<typename T> SALEM_API T GetArgument(size_t p_index);


    private:
		// This is just to calculate the size of a packet, the object is never actually used
		struct PacketHeader {
			size_t packet_size;
			int event_hook_index;
			size_t argument_count;
		};

		struct PacketArgument {
			size_t arg_size;
			void* arg_value;
		};

		size_t m_bytes_read = 0;
        int m_event_hook_index = 0;
        std::vector<PacketArgument> m_arguments;
		std::unique_ptr<Events::NetworkObserver>& m_observer;
    };
}
#endif //SALEM2D_PACKET_HPP
