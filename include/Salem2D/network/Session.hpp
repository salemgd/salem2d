#ifndef SALEM2D_SESSION_HPP
#define SALEM2D_SESSION_HPP

#include <iostream>
#include <boost/asio.hpp>

#include "Salem2D/module.h"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network {

    class Packet;
    class Server;
    class Client;

    namespace Events {
        class NetworkObserver;
    }

    class Session {
    public:
		SALEM_API Session(boost::asio::io_service &p_io_service, std::unique_ptr<Events::NetworkObserver>& p_observer, Server* p_server);
		SALEM_API Session(boost::asio::io_service &p_io_service, std::unique_ptr<Events::NetworkObserver>& p_observer, Client* p_client);

		SALEM_API boost::asio::ip::tcp::socket& get_socket() { return m_socket; }

		SALEM_API void BeginRead();
		SALEM_API void Send(Packet& p_packet);
		SALEM_API void handle_packet_received(const boost::system::error_code &p_error, size_t p_bytes_read);
		SALEM_API void handle_packet_sent(const boost::system::error_code &p_error, size_t p_bytes_sent);

		SALEM_API std::unique_ptr<Events::NetworkObserver>& get_observer(){return m_observer;}

		SALEM_API void set_index(long long p_index) { m_index = p_index; }
		SALEM_API long long get_index() { return m_index; }

		SALEM_API Server* get_server(){ return m_server; }
		SALEM_API Client* get_client(){ return m_client; }
    private:
        boost::asio::ip::tcp::socket m_socket;
		std::unique_ptr<Events::NetworkObserver>& m_observer;
        enum { max_length = 1024 };
        char data[max_length];

        std::vector<char> m_chunk_data;

        long long m_index = -1;
        Server* m_server;
        Client* m_client;
    };
}
#endif //SALEM2D_SESSION_HPP
