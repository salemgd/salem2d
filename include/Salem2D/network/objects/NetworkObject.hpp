#include "Salem2D/module.h"
#include "Salem2D/network/Session.hpp"
#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/network/Packet.hpp"

#ifndef SALEM2D_NETWORKOBJECT_HPP
#define SALEM2D_NETWORKOBJECT_HPP

namespace Salem2D::System { class GameScene; }
namespace Salem2D::Network::System { class NetworkLoop; }
namespace Salem2D::Network::Objects { class NetworkMap; }

namespace Salem2D::Network::Objects {
    class NetworkObject : public Salem2D::Objects::GameObject {
    public:
        SALEM_API NetworkObject(std::string p_object_id, float p_x, float p_y, float p_z, unsigned int p_object_type=0)
            : Salem2D::Objects::GameObject(p_x, p_y, p_z, std::move(p_object_id)), m_object_type(p_object_type), m_session(nullptr) {}

        SALEM_API NetworkObject(std::string p_object_id, float p_x, float p_y, float p_z, Salem2D::Network::Session* p_session, unsigned int p_object_type=0)
            : Salem2D::Objects::GameObject(p_x, p_y, p_z, std::move(p_object_id)), m_object_type(p_object_type), m_session(p_session) {}

        SALEM_API Salem2D::Network::Session* get_session() { return m_session; }

        SALEM_API virtual void Initialize() override { Salem2D::Objects::GameObject::Initialize(); }
        SALEM_API virtual void LateUpdate() override {}
        SALEM_API void Update(Salem2D::System::GameScene* m_game_scene) override {}

        SALEM_API virtual void Update(Salem2D::Network::System::NetworkLoop* p_network_loop, Salem2D::Network::Objects::NetworkMap* p_network_map) override
        {
            Salem2D::Objects::GameObject::Update(p_network_loop, p_network_map);
        }

        SALEM_API virtual void FixedUpdate() override {}
        SALEM_API void Draw(sf::RenderWindow& p_window, sf::IntRect p_camera) override {}
        SALEM_API unsigned int object_type() { return m_object_type; }

        SALEM_API virtual Packet& PushToPacket(Packet& p_packet) {
            float x = transform().x();
            float y = transform().y();
            float z = transform().z();
            unsigned int obj_type = object_type();

            p_packet.AddArgument(sizeof(unsigned int), &obj_type);
            p_packet.AddArgument(object_id());

            p_packet.AddArgument(sizeof(float), &x);
            p_packet.AddArgument(sizeof(float), &y);
            p_packet.AddArgument(sizeof(float), &z);

            return p_packet;
        }
    private:
        unsigned int m_object_type = 0;
        Salem2D::Network::Session* m_session;
    };
}
#endif //SALEM2D_NETWORKOBJECT_HPP
