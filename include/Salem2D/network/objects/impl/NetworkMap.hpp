#ifndef SALEM2D_NETWORKMAP_HPP
#define SALEM2D_NETWORKMAP_HPP

#include "Salem2D/network/Session.hpp"
#include <map>
#include <memory>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include "Salem2D/network/objects/NetworkObject.hpp"

namespace Salem2D::Network { class Packet;}
namespace Salem2D::Network::System{ class NetworkLoop;}

namespace Salem2D::Network::Objects {
    struct NetworkTile {
        int gid;
        int blockType = 0;
    };

    class NetworkMap {
    public:
        SALEM_API NetworkMap(std::string p_map_file);

        SALEM_API virtual void Update(Salem2D::Network::System::NetworkLoop* p_loop) {
            for ( auto &[key, value] : m_object_ptrs ) {
                value->Update(p_loop, this);
            }
        }

        SALEM_API void SendToMap(Packet& packet);

        SALEM_API std::map<std::string, std::shared_ptr<Salem2D::Network::Objects::NetworkObject>>& object_ptrs() {
            return m_object_ptrs;
        }

        SALEM_API unsigned int GetTileGid(int x, int y)
        {
            size_t index = (y*m_tile_width) + x;
            if(index < m_playground_data.size())
            {
                return m_playground_data[index];
            }

            return 0;
        }

        SALEM_API int GetTileBlockType(int gid)
        {
            if(m_tiles.find(gid) != m_tiles.end())
            {
                return m_tiles[gid].blockType;
            }

            return 0;
        }

        template<class T>
        SALEM_API std::shared_ptr<T> SpawnNetworkObject(std::string p_object_id, T* p_game_object)
        {
            p_game_object->Initialize();
            m_object_ptrs.emplace(p_object_id, std::move(p_game_object));
            return std::dynamic_pointer_cast<T>(m_object_ptrs[p_object_id]);
        }

        template<class T>
        SALEM_API std::shared_ptr<T> FindNetworkObject(std::string p_object_id)
        {
            if(m_object_ptrs.find(p_object_id) != m_object_ptrs.end())
            {
                return std::dynamic_pointer_cast<T>(m_object_ptrs[p_object_id]);
            }

            return std::dynamic_pointer_cast<T>(m_null_object_ptr);
        }
        SALEM_API void DestroyNetworkObject(const char* p_object_id)
        {
            if(m_object_ptrs.find(p_object_id) != m_object_ptrs.end())
            {
                m_object_ptrs.erase(p_object_id);
            }
        }

    private:
        std::string m_map_file;
        std::map<int, NetworkTile> m_tiles;

        unsigned int m_tile_width, m_tile_height;
        unsigned int m_map_width, m_map_height;
        std::vector<unsigned int> m_playground_data;

        std::map<std::string, std::shared_ptr<Salem2D::Network::Objects::NetworkObject>> m_object_ptrs;
        std::shared_ptr<Salem2D::Network::Objects::NetworkObject> m_null_object_ptr;

        // Boost does not support json to arrays, this method will convert json arrays to vectors for us.
        template <typename T>
        std::vector<T> as_vector(boost::property_tree::ptree const& pt, boost::property_tree::ptree::key_type const& key)
        {
            std::vector<T> r;
            for(auto& item : pt.get_child(key))
                r.push_back(item.second.get_value<T>());
            return r;
        }
    };
}

#endif //SALEM2D_NETWORKMAP_HPP
