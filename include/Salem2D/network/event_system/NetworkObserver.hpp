#ifndef SALEM2D_NETWORKOBSERVER_HPP
#define SALEM2D_NETWORKOBSERVER_HPP

#include <map>
#include <string>
#include <iostream>
#include <vector>
#include <mutex>

#include "Salem2D/module.h"
#include "Salem2D/network/event_system/NetworkEvent.hpp"

namespace Salem2D::Network {

    class Packet;
    class Server;
    class Client;
    class Session;

    namespace Events {
        class NetworkObserver {
        public:
			SALEM_API NetworkObserver() : m_hook_map(), m_reverse_hook_map(), m_subscribers(), m_hook_map_mutex() {}
			SALEM_API void Trigger(std::string p_hook, Packet& p_packet, Salem2D::Network::Session* p_session);

			SALEM_API void SubscribeEvent(std::string p_hook, NetworkEvent* p_network_event);

			SALEM_API int get_hook_index(std::string p_hook);
			SALEM_API std::string get_hook(int p_hook_index);

			SALEM_API void print_hook_map()
            {
                std::cout << "hookmap: " << std::endl;
                for (auto it=m_hook_map.begin(); it!=m_hook_map.end(); ++it)
                    std::cout << it->first << " => " << it->second << '\n';

                std::cout << "reverse hookmap: " << std::endl;
                for (auto it=m_reverse_hook_map.begin(); it!=m_reverse_hook_map.end(); ++it)
                    std::cout << it->first << " => " << it->second << '\n';
            }
        private:
            int append_hook_to_map(std::string p_hook);

            int m_hook_index = 0;
            std::map<std::string, int> m_hook_map;
            std::map<int, std::string> m_reverse_hook_map;
            std::vector<NetworkEvent*> m_subscribers;
			std::mutex m_hook_map_mutex;
        };
    }
}

#endif // SALEM2D_NETWORKOBSERVER_HPP
