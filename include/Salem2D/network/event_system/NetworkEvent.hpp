#ifndef SALEM2D_NETWORKSUBSCRIBER_HPP
#define SALEM2D_NETWORKSUBSCRIBER_HPP

#include <string>
#include "Salem2D/module.h"

namespace Salem2D::Network {

    class Packet;
    class Server;
    class Session;

    namespace Events {
        class NetworkObserver;

        class NetworkEvent {
        public:
			SALEM_API void set_hook(std::string p_hook) { m_hook = p_hook; }
			SALEM_API std::string &get_hook() { return m_hook; }
			SALEM_API virtual void Call(Packet&, Salem2D::Network::Session*);
        private:
            std::string m_hook;
        };
    }
}

#endif //SALEM2D_NETWORKSUBSCRIBER_HPP
