#ifndef SALEM2D_CLIENT_HPP
#define SALEM2D_CLIENT_HPP

#include <thread>
#include <boost/asio.hpp>

#include "Salem2D/module.h"
#include "Salem2D/network/Session.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network {
    class Client {
    public:
		SALEM_API Client();

		SALEM_API ~Client() { m_client_thread.join(); }
		SALEM_API void connect(const char* p_hostname, unsigned short p_port);
		SALEM_API void client_thread();

		SALEM_API std::unique_ptr<Events::NetworkObserver>& get_observer(){ return m_observer; }
		SALEM_API Session& get_session() { return m_session; }
    private:
		boost::asio::io_service m_io_service;
		std::unique_ptr<Events::NetworkObserver> m_observer;
        Session m_session;
        std::thread m_client_thread;
    };
}

#endif //SALEM2D_CLIENT_HPP
