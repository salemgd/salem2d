#ifndef SALEM2D_NETWORKLOOP_HPP
#define SALEM2D_NETWORKLOOP_HPP

#include <vector>
#include <iostream>
#include <thread>
#include <boost/filesystem.hpp>

#include "Salem2D/module.h"
#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/network/objects/impl/NetworkMap.hpp"

namespace Salem2D::Network::System {
    class NetworkLoop {
    public:
		SALEM_API explicit NetworkLoop(unsigned int p_time_step_ms) : m_time_step(p_time_step_ms) {
            Initialize();
        }

		SALEM_API ~NetworkLoop() {
            m_running = false;
            m_run_thread.join();
            Shutdown();
        }

		SALEM_API void Run();
		SALEM_API void RunThread();

		SALEM_API virtual void Initialize(){
		    LoadAllMaps("data/maps");
		}
		SALEM_API virtual void Tick(){
			// Update Game Objects
			for ( auto &[key, value] : m_network_maps ) {
				value->Update(this);
			}
		}

		SALEM_API virtual void Shutdown() {}
        SALEM_API std::unique_ptr<Salem2D::Network::Objects::NetworkMap>& get_map(std::string map_file) {
		    if(m_network_maps.find(map_file) != m_network_maps.end())
            {
                return m_network_maps[map_file];
            }

            return m_null_map_ptr;
		}

    private:
		void LoadAllMaps(const char * p_map_directory) {
		    // TODO: try catch
            namespace fs = boost::filesystem;

            std::cout << "Loading maps from files..." << std::endl;
            fs::directory_iterator begin(p_map_directory), end;
            std::vector<fs::directory_entry> dir(begin, end);

            for(auto& file: dir)
            {
                auto path = std::string(file.path().c_str());
                if(path.find(".json") != std::string::npos)
                {
                    m_network_maps.emplace(path, std::move(std::make_unique<Salem2D::Network::Objects::NetworkMap>(path)));

                    std::cout << "[Maps] Loaded " << path << std::endl;
                }
            }

            std::cout << "Finished loading maps from files..." << std::endl;
		}

        unsigned int m_time_step;
        bool m_running = false;
        std::thread m_run_thread;

		std::map<std::string, std::unique_ptr<Salem2D::Network::Objects::NetworkMap>> m_network_maps;

        std::unique_ptr<Salem2D::Network::Objects::NetworkMap> m_null_map_ptr;
    };
}

#endif //SALEM2D_NETWORKLOOP_HPP
