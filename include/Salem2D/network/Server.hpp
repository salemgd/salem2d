#ifndef SALEM2D_SERVER_HPP
#define SALEM2D_SERVER_HPP

#include <map>
#include <memory>
#include <vector>
#include <boost/asio.hpp>

#include "Salem2D/module.h"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/system/NetworkLoop.hpp"

namespace Salem2D::Network {
    class Session;

    class Server {
    public:
		SALEM_API explicit Server(unsigned short p_port, Salem2D::Network::System::NetworkLoop*);
		SALEM_API void Run();
		SALEM_API void handle_accept(std::shared_ptr<Session> p_session, const boost::system::error_code& p_error);
		SALEM_API std::unique_ptr<Events::NetworkObserver>&  get_observer() { return m_observer; }

		SALEM_API std::shared_ptr<Session> get_session(long long p_session_index);
		SALEM_API void SendToAll(Packet& packet);
    private:
        boost::asio::io_service m_io_service;
        boost::asio::ip::tcp::acceptor m_acceptor;

		std::unique_ptr<Events::NetworkObserver> m_observer;

        // TODO: Protect from session spamming
        // TODO: Clear sessions on disconnect
        std::vector<std::shared_ptr<Session>> m_sessions;

        
        long long m_session_index = 0;
    };
}

#endif //SALEM2D_SERVER_HPP
