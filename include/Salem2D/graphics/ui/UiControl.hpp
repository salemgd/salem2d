#ifndef SALEM2D_UICONTROL_HPP
#define SALEM2D_UICONTROL_HPP

#include <string>

#include "Salem2D/module.h"
#include "Salem2D/physics/Transform.hpp"
#include "Salem2D/graphics/ui/ControlAttributes.hpp"
#include "Salem2D/system/objects/GameObject.hpp"

namespace Salem2D::Graphics::Ui {
    class UiControl {
    public:
        SALEM_API UiControl(ControlAttributes p_attributes, sf::FloatRect p_box_transform, std::string p_object_id)
            : m_box_transform(p_box_transform), m_object_id(std::move(p_object_id)) {}


        SALEM_API virtual void Initialize(){}
        SALEM_API virtual void Draw(sf::RenderWindow& p_window){}

        SALEM_API virtual void OnMouseEnter() {}
        SALEM_API virtual void OnMouseLeave() {}

        // Only Called if the mouse clicks on the UiControl
        SALEM_API virtual void OnMouseDown(sf::Event event) {}
        SALEM_API virtual void OnMouseUp(sf::Event event){}
        SALEM_API virtual void OnMouseScroll(sf::Event event) {}

        // Only called if the GUI object is active in the gui scene
        SALEM_API virtual void OnKeyPressed(sf::Event event) {}
        SALEM_API virtual void OnKeyReleased(sf::Event event) {}
        SALEM_API virtual void OnTextEntered(sf::Event event) {}

        SALEM_API sf::FloatRect& box_transform() { return m_box_transform; }
        SALEM_API void box_transform(sf::FloatRect p_box_transform) { m_box_transform = p_box_transform; }

        SALEM_API std::string& object_id(){ return m_object_id; }
    private:
        sf::FloatRect m_box_transform;
        std::string m_object_id;
    };
}

#endif //SALEM2D_UICONTROL_HPP
