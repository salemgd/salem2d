#ifndef SALEM2D_BUTTON_HPP
#define SALEM2D_BUTTON_HPP

#include "Salem2D/graphics/ui/UiControl.hpp"

#include <SFML/Graphics.hpp>

namespace Salem2D::Graphics::Ui::Controls {
class Button : public Salem2D::Graphics::Ui::UiControl {
    public:
        SALEM_API Button(ControlAttributes p_attributes, sf::FloatRect p_box_transform, std::string p_object_id);

        SALEM_API void Draw(sf::RenderWindow& p_window) override;
        SALEM_API void OnMouseEnter() override;
        SALEM_API void OnMouseLeave() override;
        SALEM_API void OnMouseDown(sf::Event event) override;
        SALEM_API void OnMouseUp(sf::Event event) override;

        SALEM_API void(*on_click)() = nullptr;
    private:
        std::vector<sf::Texture> m_state_textures;

        sf::Image m_image;
        sf::Sprite m_sprite;
        unsigned int m_texture_width, m_texture_height;

        sf::Text m_label;
    };
}

#endif //SALEM2D_BUTTON_HPP
