#ifndef SALEM2D_GUI_HPP
#define SALEM2D_GUI_HPP

#include <iostream>

#include <map>
#include <boost/property_tree/ptree.hpp>

#include "Salem2D/module.h"
#include "Salem2D/graphics/ui/UiControl.hpp"
#include "Salem2D/graphics/ui/ControlAttributes.hpp"

namespace Salem2D::Graphics::Ui {
    class Gui {
    public:
        SALEM_API Gui();
        SALEM_API Gui(const char* p_file);
        SALEM_API void Draw(sf::RenderWindow& p_window);

        SALEM_API virtual void OnMouseDown(sf::Event event)
        {
            if(m_hover_control.get() != nullptr)
                m_hover_control->OnMouseDown(event);
        }

        SALEM_API virtual void OnMouseUp(sf::Event event)
        {
            m_active_control.reset();

            if(m_hover_control.get() != nullptr) {
                m_hover_control->OnMouseUp(event);
                m_active_control = m_hover_control;
            }

        }

        SALEM_API virtual void OnMouseScroll(sf::Event event)
        {
            if(m_hover_control.get() != nullptr) {
                m_hover_control->OnMouseScroll(event);
            }
        }

        SALEM_API virtual void OnMouseMove(sf::Event event)
        {
            if(m_hover_control.get() != nullptr)
            {
                if(!m_hover_control->box_transform().contains(event.mouseMove.x, event.mouseMove.y))
                {
                    m_hover_control->OnMouseLeave();
                    m_hover_control = nullptr;
                }
            }

            for(auto& [key, value] : m_controls)
            {
                if(value->box_transform().contains(event.mouseMove.x, event.mouseMove.y))
                {
                    if(value.get() != m_hover_control.get())
                    {
                        value->OnMouseEnter();
                        m_hover_control = value;
                        return;
                    }
                }
            }
        }

        SALEM_API virtual void OnKeyPressed(sf::Event event)
        {
            if(m_active_control.get() != nullptr)
                m_active_control->OnKeyPressed(event);
        }

        SALEM_API virtual void OnKeyReleased(sf::Event event)
        {
            if(m_active_control.get() != nullptr)
                m_active_control->OnKeyReleased(event);
        }

        SALEM_API virtual void OnTextEntered(sf::Event event)
        {
            if(m_active_control.get() != nullptr)
                m_active_control->OnTextEntered(event);
        }

        SALEM_API void AddControl(std::shared_ptr<UiControl> p_control, int p_z_index = 0)
        {
            p_control->Initialize();
            m_controls.insert(std::make_pair(GetNextFreeZIndex(p_z_index), p_control));
        }

        template<class T>
        SALEM_API std::shared_ptr<T> FindControlById(std::string p_object_id)
        {
            for(auto const&it : m_controls)
            {
                if(it.second->object_id() == p_object_id)
                {
                    return std::dynamic_pointer_cast<T>(it.second);
                }

                return std::shared_ptr<T>(nullptr);
            }

        }

    private:
        int GetNextFreeZIndex(int p_base_z = 0)
        {
            for(int z = p_base_z; z < 99999; ++z)
            {
                if(m_controls.find(p_base_z) == m_controls.end())
                {
                    return z;
                }
            }

            return -1;
        }

        std::shared_ptr<sf::Font> FindFontById(std::string p_font_id)
        {
            if(m_fonts.find(p_font_id) != m_fonts.end())
            {
                return m_fonts.at(p_font_id);
            }

            return std::shared_ptr<sf::Font>(nullptr);
        }

        std::shared_ptr<UiControl> CreateControl(std::string& p_object_type, std::string& p_object_id, boost::property_tree::ptree::value_type& p_control_data, int p_z_index);

        template<class T>
        std::shared_ptr<T> GenerateControl(std::string& p_object_id, boost::property_tree::ptree::value_type& p_control_data, int p_z_index);
        ControlAttributes LoadAttributes(boost::property_tree::ptree::value_type& p_control_data);

        std::map<int, std::shared_ptr<UiControl>> m_controls;
        std::map<std::string, std::shared_ptr<sf::Font>> m_fonts;
        std::shared_ptr<UiControl> m_active_control;
        std::shared_ptr<UiControl> m_hover_control;
    };
}

#endif //SALEM2D_GUI_HPP
