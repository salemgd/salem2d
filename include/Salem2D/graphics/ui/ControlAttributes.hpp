#ifndef SALEM2D_CONTROLATTRIBUTES_HPP
#define SALEM2D_CONTROLATTRIBUTES_HPP

#include <string>
#include <SFML/Graphics.hpp>

namespace Salem2D::Graphics::Ui {
    struct ControlAttributes {
        std::shared_ptr<sf::Font> font = nullptr;
        std::string text_align = "";
        std::string anchor = "";
        std::string place_holder = "";
        //Physics::Vector3 font_color = Physics::Vector3(0.0f, 0.0f, 0.0f);
        std::string label = "";
        bool is_password = false;
        unsigned int font_size = 12;
        std::vector<std::string> textures;
    };

}

#endif //SALEM2D_CONTROLATTRIBUTES_HPP
