#ifndef SALEM2D_TILESHEET_HPP
#define SALEM2D_TILESHEET_HPP

#include <memory>
#include <SFML/Graphics.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "Salem2D/module.h"

namespace Salem2D::Graphics {
    enum class SALEM_API TileType {
        walkable = 0,
        block = 1,
        platform = 2,
        water = 3,
        kill = 4
    };

    class Tile{
    public:
        Tile(std::shared_ptr<sf::Texture> p_texture, TileType p_tile_type)
                : m_texture(std::move(p_texture)), m_tile_type(p_tile_type) {}

        std::shared_ptr<sf::Texture> SALEM_API get_texture(){ return m_texture; }
        TileType SALEM_API get_tile_type()  { return m_tile_type; }
    private:
		std::shared_ptr<sf::Texture> m_texture;
        TileType m_tile_type;
    };

    class TileSheet {
    public:
        TileSheet(const char* p_tilesheet_path, int p_first_gid);

        std::shared_ptr<sf::Texture> SALEM_API GetTileTexture(int p_gid){ return m_tile_sprite_matrix[p_gid-1].get_texture(); }
        TileType SALEM_API GetTileType(int p_gid){ return m_tile_sprite_matrix[p_gid-1].get_tile_type(); }

        int SALEM_API get_first_gid(){return m_first_gid;}
        int SALEM_API get_tile_count(){return m_tile_count;}
    private:
        int m_tile_width, m_tile_height, m_tiles_x, m_tiles_y, m_first_gid, m_tile_count;
        int m_gid_on = 0;
        sf::Image m_image;

        std::vector<Tile> m_tile_sprite_matrix;
    };


}

#endif //SALEM2D_TILESHEET_HPP
