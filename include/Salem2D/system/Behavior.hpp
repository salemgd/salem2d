#ifndef SALEM2D_BEHAVIOR_HPP
#define SALEM2D_BEHAVIOR_HPP

#include <memory>
#include "Salem2D/module.h"

namespace Salem2D::Objects { class GameObject; }
namespace Salem2D::System { class GameScene; }
namespace Salem2D::Network::System { class NetworkLoop; }
namespace Salem2D::Network::Objects { class NetworkMap; }

namespace Salem2D::System {
    class SALEM_API Behavior {
    public:
        virtual void Destroy();
        virtual void Initialize();
        virtual void Update(Salem2D::System::GameScene* m_game_scene, Salem2D::Objects::GameObject* p_game_object);
        virtual void Update(Salem2D::Network::System::NetworkLoop* m_network_loop, Salem2D::Network::Objects::NetworkMap* p_map, Salem2D::Objects::GameObject* p_game_object);
        virtual void FixedUpdate();
        virtual void LateUpdate();
        virtual void OnBehaviorAttached();
    };
}

#endif //SALEM2D_BEHAVIOR_HPP
