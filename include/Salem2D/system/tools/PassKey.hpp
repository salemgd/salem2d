#ifndef SALEM2D_PASSKEY_HPP
#define SALEM2D_PASSKEY_HPP

namespace Salem2D::Tools {
// This is used to make sure that only Map may create a MapCapsule object
    template<typename T>
    class PassKey {
    private:
        friend T;
        PassKey(){}
        PassKey(const PassKey &) {}
        PassKey &operator=(const PassKey &) = delete;
    };
}
#endif //SALEM2D_PASSKEY_HPP
