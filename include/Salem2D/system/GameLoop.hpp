#ifndef SALEM2D_GAMELOOP_HPP
#define SALEM2D_GAMELOOP_HPP

#include <vector>
#include <SFML/Graphics.hpp>

#include "GameScene.hpp"
#include "Salem2D/module.h"

namespace Salem2D::System {
    class GameLoop {
    public:
		SALEM_API explicit GameLoop(GameScene* p_gamescene)
                : m_window(sf::VideoMode(480, 320, 32), "Salem2D"),
                  m_scene_ptr(p_gamescene)
        {
            m_scene_ptr->main_window(&m_window);
            m_scene_ptr->Initialize();
        }

		SALEM_API void Run();

        SALEM_API std::unique_ptr<GameScene>& scene_ptr() { return m_scene_ptr; }
    private:
        void Draw(sf::RenderWindow& p_window);
        void Update();
        void LateUpdate();
        void FixedUpdate();

        void HandleEvents();

        std::unique_ptr<GameScene> m_scene_ptr;
        sf::RenderWindow m_window;
    };
}

#endif //SALEM2D_GAMELOOP_HPP
