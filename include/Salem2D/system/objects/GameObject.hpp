#ifndef SALEM2D_GAMEOBJECT_HPP
#define SALEM2D_GAMEOBJECT_HPP

#include <vector>
#include <SFML/Graphics.hpp>

#include "Salem2D/physics/Transform.hpp"
#include "Salem2D/system/Behavior.hpp"
#include "Salem2D/module.h"

namespace Salem2D::Network::Objects { class NetworkMap; }

namespace Salem2D::Objects {
    class GameObject {
    public:
		SALEM_API GameObject(float p_x, float p_y, float p_z, std::string p_object_id)
			: m_transform(p_x, p_y, p_z), m_object_id(std::move(p_object_id)) {}

        SALEM_API ~GameObject();

		SALEM_API virtual void Initialize();
		SALEM_API virtual void LateUpdate();
		SALEM_API virtual void Update(Salem2D::System::GameScene* m_game_scene);
		SALEM_API virtual void Update(Salem2D::Network::System::NetworkLoop* p_network_loop, Salem2D::Network::Objects::NetworkMap* p_network_map);
		SALEM_API virtual void FixedUpdate();
		SALEM_API virtual void Draw(sf::RenderWindow& p_window, sf::IntRect p_camera);

		SALEM_API void AttachBehavior(std::unique_ptr<Salem2D::System::Behavior> p_behavior);

		SALEM_API std::string& object_id(){ return m_object_id; }
		SALEM_API Salem2D::Physics::Transform& transform() { return m_transform; }
    private:
        Salem2D::Physics::Transform m_transform;
        std::vector<std::unique_ptr<Salem2D::System::Behavior>> m_behaviors;
        std::string m_object_id;
    };
}

#endif //SALEM2D_GAMEOBJECT_HPP
