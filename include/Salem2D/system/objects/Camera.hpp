//
// Created by Dylan Dodds on 7/17/18.
//

#ifndef SALEM2D_CAMERA_HPP
#define SALEM2D_CAMERA_HPP

#include "Salem2D/module.h"

namespace Salem2D::Objects{
    struct SALEM_API Camera{
        int x;
        int y;
        int width;
        int height;
    };
}

#endif //SALEM2D_CAMERA_HPP
