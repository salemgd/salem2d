#ifndef SALEM2D_GAMEWORLD_HPP
#define SALEM2D_GAMEWORLD_HPP

#include <map>
#include <vector>
#include <string>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Salem2D/module.h"

namespace Salem2D::Objects {
	class Map;
    struct SALEM_API WorldTransform {
        double x;
        double y;
        int map_x;
        int map_y;
        double width;
        double height;
    };

    class GameWorld {
    public:
		SALEM_API GameWorld(const char * p_world_file);
		SALEM_API void DrawMask(sf::RenderWindow &p_window, sf::IntRect p_view);
		SALEM_API void DrawFringe(sf::RenderWindow &p_window, sf::IntRect p_view);
		SALEM_API void Initialize(sf::IntRect p_initial_view);
    private:
        const char* m_world_file;
        std::map<WorldTransform, std::string> m_map_matrix;
        std::vector<std::unique_ptr<Salem2D::Objects::Map>> m_loaded_map_matrix;
        sf::IntRect ConvertToLocalView(sf::IntRect p_view);
    };
}

#endif //SALEM2D_GAMEWORLD_HPP
