//
// Created by Dylan Dodds on 7/17/18.
//

#ifndef SALEM2D_MAPLAYER_HPP
#define SALEM2D_MAPLAYER_HPP

#include <SFML/Graphics.hpp>

#include "Salem2D/system/objects/Camera.hpp"
#include "Salem2D/graphics/TileSheet.hpp"
#include "Salem2D/system/objects/MapCapsule.hpp"
#include "Salem2D/module.h"

namespace Salem2D::Objects {
    class MapLayer {
    public:
		SALEM_API MapLayer(std::string p_layer_name,
                 unsigned int p_map_width, unsigned int p_map_height,
                 unsigned int p_tile_width, unsigned int p_tile_height,
                 float p_opacity, bool p_visible);

		SALEM_API void DrawTexture(std::unique_ptr<MapCapsule>& m_map_capsule, std::vector<unsigned int> p_data);

		SALEM_API std::string get_layer_name() { return m_layer_name; }
		SALEM_API sf::RenderTexture& get_render_texture() { return m_render_texture; }
		SALEM_API bool get_visible(){return this->m_visible;}
    private:
        unsigned int m_tile_height, m_tile_width, m_map_width, m_map_height = 0;
        float m_opacity = 0;
        bool m_visible = true;
        std::string m_layer_name;
        sf::RenderTexture m_render_texture;
    };
}

#endif //SALEM2D_MAPLAYER_HPP
