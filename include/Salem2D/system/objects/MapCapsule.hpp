//
// Created by oddlydoddly on 7/19/2018.
//

#ifndef SALEM2D_MAPCAPSULE_HPP
#define SALEM2D_MAPCAPSULE_HPP

#include "Salem2D/system/tools/PassKey.hpp"

namespace Salem2D::Objects {
class Map;

// This is used to encapsulate MapData that we want to pass of to MapLayers
class MapCapsule{
    public:
        explicit MapCapsule() {}
        MapCapsule(const MapCapsule&) = delete; // Disallow Copying
        Salem2D::Graphics::TileSheet* get_tile_sheet_raw_pointer (size_t p_index){
            return m_tile_sheets[p_index].get();
        };

        size_t get_tile_sheet_count() { return m_tile_sheets.size(); }
    private:
        friend class Map; // Allow Map access to these private methods
        std::vector<std::unique_ptr<Salem2D::Graphics::TileSheet>> m_tile_sheets;
        void append_tile_sheet(std::unique_ptr<Salem2D::Graphics::TileSheet> p_tilesheet) {
            m_tile_sheets.push_back(std::move(p_tilesheet));
        }

    };
}

#endif //SALEM2D_MAPCAPSULE_HPP
