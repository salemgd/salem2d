//
// Created by Dylan Dodds on 7/10/18.
//

#ifndef SALEM2D_SPRITE_HPP
#define SALEM2D_SPRITE_HPP

#include "Salem2D/module.h"
#include "Salem2D/system/objects/GameObject.hpp"

#include <vector>
#include <SFML/Graphics.hpp>

namespace Salem2D::Objects::Impl {
    class Puppet : public Salem2D::Objects::GameObject {
    public:
		SALEM_API Puppet(std::string object_id, float p_x, float p_y, float p_z,
                const char* p_sprite_sheet_path, int p_sprite_width, int p_sprite_height);

		SALEM_API void Update(Salem2D::System::GameScene* m_game_scene) override;
		SALEM_API void Draw(sf::RenderWindow& p_window, sf::IntRect p_camera) override;
		SALEM_API void SetAnimation(int p_animation) { m_animation = p_animation; }

    private:
        int m_frame = 0, m_animation = 0;
        int m_max_frames, m_max_animations;
        int m_sprite_width, m_sprite_height;

        sf::Sprite m_sprite;
        sf::Image m_image;
        std::vector<std::vector<sf::Texture>> m_animation_texture_matrix;
    };
}

#endif //SALEM2D_SPRITE_HPP
