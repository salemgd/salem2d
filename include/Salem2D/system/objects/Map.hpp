#ifndef SALEM2D_HPP
#define SALEM2D_HPP

#include <vector>
#include <iostream>

#include <SFML/Graphics.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "Salem2D/module.h"
#include "Salem2D/system/objects/MapLayer.hpp"
#include "Salem2D/graphics/TileSheet.hpp"
#include "Salem2D/system/tools/PassKey.hpp"


namespace Salem2D::Objects {
    class Map {
    public:
		SALEM_API explicit Map(const char* p_map_file);
		SALEM_API ~Map() {
            std::cout << "Map is being delete" << std::endl;
        }

		SALEM_API void DrawMask(sf::RenderWindow &p_window, sf::IntRect p_view);
		SALEM_API void DrawFringe(sf::RenderWindow &p_window, sf::IntRect p_view);

		SALEM_API unsigned int get_width() { return m_map_width*m_tile_width; }
		SALEM_API unsigned int get_height() { return m_map_height*m_tile_height; }

		SALEM_API MapCapsule* get_capsule() { return m_map_capsule.get(); }
		SALEM_API bool get_is_drawn() { return m_is_drawn; }
    private:
        void RenderBackground();
        void RenderForeground();

        std::unique_ptr<MapCapsule> m_map_capsule;

        unsigned int m_tile_width, m_tile_height = 32;
        unsigned int m_map_width, m_map_height = 0;

        std::vector<std::unique_ptr<Salem2D::Objects::MapLayer>> m_layers;

        sf::RenderTexture* m_mask_render_texture;
        sf::RenderTexture* m_fringe_render_texture;

        // Boost does not support json to arrays, this method will convert json arrays to vectors for us.
        template <typename T>
        std::vector<T> as_vector(boost::property_tree::ptree const& pt, boost::property_tree::ptree::key_type const& key)
        {
            std::vector<T> r;
            for(auto& item : pt.get_child(key))
                r.push_back(item.second.get_value<T>());
            return r;
        }

        bool m_is_drawn = false;
    };
}

#endif //SALEM2D_HPP
