//
// Created by Dylan Dodds on 7/10/18.
//

#ifndef SALEM2D_GAMESCENE_HPP
#define SALEM2D_GAMESCENE_HPP

#include <vector>
#include <SFML/Graphics.hpp>

#include "Salem2D/module.h"
#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/system/objects/Map.hpp"
#include "Salem2D/graphics/ui/Gui.hpp"

namespace Salem2D::System {
    class GameScene {
    public:
        GameScene() : m_camera(0, 0, 0, 0), m_gui() {}
		SALEM_API virtual ~GameScene() {}

		// Over-loadables
		SALEM_API virtual void Update();
		SALEM_API virtual void LateUpdate();
		SALEM_API virtual void FixedUpdate();
		SALEM_API virtual void Initialize();
		SALEM_API virtual void Draw(sf::RenderWindow& p_window);

		SALEM_API void LoadGui(const char * p_gui_file) {
            m_gui = Salem2D::Graphics::Ui::Gui(p_gui_file);
		}

		// Events
		SALEM_API virtual void OnKeyPressed(sf::Event event) {
			m_gui.OnKeyPressed(event);
		}

		SALEM_API virtual void OnKeyReleased(sf::Event event) {
            m_gui.OnKeyReleased(event);
		}

		SALEM_API virtual void OnMouseDown(sf::Event event) {
			m_gui.OnMouseDown(event);
		}

		SALEM_API virtual void OnMouseUp(sf::Event event) {
            m_gui.OnMouseUp(event);
		}

		SALEM_API virtual void OnMouseMove(sf::Event event) {
			m_gui.OnMouseMove(event);
		}

		SALEM_API virtual void OnMouseScroll(sf::Event event) {
			m_gui.OnMouseScroll(event);
		}

		SALEM_API virtual void OnTextEntered(sf::Event event)
        {
		    m_gui.OnTextEntered(event);
        }

        SALEM_API virtual void OnLostFocus(sf::Event event) {}
        SALEM_API virtual void OnGainedFocus(sf::Event event) {}
        SALEM_API virtual void OnMouseEnteredWindow(sf::Event event) {}
        SALEM_API virtual void OnMouseLeftWindow(sf::Event event) {}
		SALEM_API virtual void OnWindowResize(sf::Event event) {}

		// TODO: GET RID OF RAW POINTER
		SALEM_API Salem2D::Objects::Map* map() { return m_loaded_map.get(); }

		SALEM_API std::unique_ptr<Salem2D::Objects::GameObject>& SpawnGameObject(std::string p_object_id, std::unique_ptr<Salem2D::Objects::GameObject> p_game_object);
		SALEM_API std::unique_ptr<Salem2D::Objects::GameObject>& FindGameObject(std::string p_object_id);

		SALEM_API void set_camera(sf::IntRect p_rect) { m_camera = p_rect; }
		SALEM_API void LoadMap(const char* map_file);

		// TODO: GET RID OF RAW POINTER
		SALEM_API void main_window(sf::RenderWindow *p_window) { m_window = p_window; }

		// TODO: GET RID OF RAW POINTER
		SALEM_API sf::RenderWindow* main_window(){ return m_window; }
		SALEM_API bool IsInView(std::unique_ptr<Salem2D::Objects::GameObject>& p_game_object);

        Salem2D::Graphics::Ui::Gui& gui() { return m_gui; }
    private:
        Salem2D::Graphics::Ui::Gui m_gui;

        // TODO: Change Game Objects to use shared ptr
        std::map<std::string, std::unique_ptr<Salem2D::Objects::GameObject>> m_object_ptrs;
        std::unique_ptr<Salem2D::Objects::Map> m_loaded_map = nullptr;
        sf::RenderWindow* m_window = nullptr;
        sf::IntRect m_camera;

        // TODO: Remove this shit
        std::unique_ptr<Salem2D::Objects::GameObject> m_null_game_ptr = nullptr;
    };
}

#endif //SALEM2D_GAMESCENE_HPP
