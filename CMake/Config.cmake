# Check that the architecture is 64-bit
include(CheckTypeSize)
check_type_size(void* SIZEOF_VOID_PTR)
if(${SIZEOF_VOID_PTR} STREQUAL "8")

    set(ARCH_64BITS 1)
else()
    message(FATAL_ERROR "${SIZEOF_VOID_PTR}-byte is not supported.")
    return()

endif()

if ((${CMAKE_SYSTEM_NAME} STREQUAL "Windows") OR MINGW OR WIN32)

    set(SALEM2D_PLATFORM "windows")
    set(SALEM2D_PLATFORM_WINDOWS 1)

elseif(${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND NOT ANDROID)

    set(SALEM2D_PLATFORM "linux")
    set(SALEM2D_PLATFORM_LINUX 1)
    set(SALEM2D_PLATFORM_UNIX 1)

elseif(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin" AND APPLE AND NOT IOS)

    set(SALEM2D_PLATFORM "mac")
    set(SALEM2D_PLATFORM_MAC 1)

elseif(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin" AND IOS)

    set(SALEM2D_PLATFORM "ios")
    set(SALEM2D_PLATFORM_IOS 1)
    message(FATAL_ERROR "iOS is not supported as a target system.")
    return()

elseif(${CMAKE_SYSTEM_NAME} STREQUAL "Android" OR (${CMAKE_SYSTEM_NAME} STREQUAL "Linux" AND ANDROID))

    set(SALEM2D_PLATFORM "android")
    set(SALEM2D_PLATFORM_ANDROID 1)
    message(FATAL_ERROR "Android is not supported as a target system.")
    return()

else()

    message(FATAL_ERROR "Unsupported platform '${CMAKE_SYSTEM_NAME}'.")
    return()

endif()
