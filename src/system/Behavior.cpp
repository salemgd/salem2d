#include "Salem2D/system/Behavior.hpp"
#include "Salem2D/system/GameScene.hpp"
#include "Salem2D/system/objects/GameObject.hpp"

namespace Salem2D::System {
    void Behavior::Destroy() {}
    void Behavior::Initialize() {}
    void Behavior::Update(Salem2D::System::GameScene* m_game_scene, Salem2D::Objects::GameObject* p_game_object) {}
    void Behavior::Update(Salem2D::Network::System::NetworkLoop* m_network_loop, Salem2D::Network::Objects::NetworkMap* p_map, Salem2D::Objects::GameObject* p_game_object) {}
    void Behavior::FixedUpdate() {}
    void Behavior::LateUpdate() {}
    void Behavior::OnBehaviorAttached() {}
}
