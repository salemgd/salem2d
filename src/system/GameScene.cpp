#include <vector>
#include <SFML/Graphics.hpp>

#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/system/objects/Map.hpp"
#include "Salem2D/system/GameScene.hpp"

namespace Salem2D::System {
    void GameScene::Update()
    {
        for(const auto& pair : m_object_ptrs)
        {
            pair.second->Update(this);
        }
    }

    void GameScene::LateUpdate()
    {
        for(const auto& pair : m_object_ptrs)
        {
            pair.second->LateUpdate();
        }
    }

    void GameScene::FixedUpdate()
    {
        for(const auto& pair : m_object_ptrs)
        {
            pair.second->FixedUpdate();
        }
    }

    void GameScene::Initialize()
    {
        for(const auto& pair : m_object_ptrs)
        {
            pair.second->Initialize();
        }
    }

    void GameScene::Draw(sf::RenderWindow& p_window)
    {
        if(m_loaded_map != nullptr)
            m_loaded_map->DrawMask(p_window, m_camera);

        for(auto& pair : m_object_ptrs)
        {
            if(IsInView(pair.second)) // Only draw if we can see the object
                pair.second->Draw(p_window, m_camera);
        }

        if(m_loaded_map != nullptr)
            m_loaded_map->DrawFringe(p_window, m_camera);

        // Draw GUI
        m_gui.Draw(p_window);
    }

    std::unique_ptr<Salem2D::Objects::GameObject>& GameScene::SpawnGameObject(std::string p_object_id, std::unique_ptr<Salem2D::Objects::GameObject> p_game_object)
    {
        p_game_object->Initialize();
        m_object_ptrs.insert(std::make_pair(p_object_id, std::move(p_game_object)));

        return m_object_ptrs[p_object_id];
    }

    std::unique_ptr<Salem2D::Objects::GameObject>& GameScene::FindGameObject(std::string p_object_id)
    {
        auto it = m_object_ptrs.find(p_object_id);

        if(it != m_object_ptrs.end())
        {
            return it->second;
        }

        return m_null_game_ptr;
    }

    void GameScene::LoadMap(const char* map_file){
        m_loaded_map = std::make_unique<Salem2D::Objects::Map>(map_file);
    }

    bool GameScene::IsInView(std::unique_ptr<Salem2D::Objects::GameObject>& p_game_object) {
        return(p_game_object->transform().x() > m_camera.left &&
           p_game_object->transform().x() < m_camera.left + m_camera.width &&
            p_game_object->transform().y() > m_camera.top &&
           p_game_object->transform().y() < m_camera.top + m_camera.height);
    }
}
