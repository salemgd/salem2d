#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/system/objects/impl/Puppet.hpp"

#include <vector>
#include <SFML/Graphics.hpp>

namespace Salem2D::Objects::Impl {
    Puppet::Puppet(std::string object_id, float p_x, float p_y, float p_z,
            const char* p_sprite_sheet_path, int p_sprite_width, int p_sprite_height)
            : GameObject(p_x, p_y, p_z, std::move(object_id)), m_sprite_width(p_sprite_width), m_sprite_height(p_sprite_height)
    {
        m_image.loadFromFile(p_sprite_sheet_path);

        m_max_frames = m_image.getSize().x/p_sprite_width;
        m_max_animations = m_image.getSize().y/p_sprite_height;

        for(int i = 0; i < m_max_animations; ++i)
        {
            std::vector<sf::Texture> texture_map;
            for(int j = 0; j < m_max_frames; ++j)
            {
                sf::IntRect texture_rect(p_sprite_width*j, p_sprite_height*i, p_sprite_width, p_sprite_height);
                sf::Texture texture;
                texture.loadFromImage(m_image, texture_rect);
                texture_map.push_back(texture);
            }

            m_animation_texture_matrix.push_back(texture_map);
        }
    }

    void Puppet::Update(Salem2D::System::GameScene* m_game_scene)
    {
        ++m_frame;
        if(m_frame >= m_max_frames) { m_frame = 0; }
        m_sprite.setTexture(m_animation_texture_matrix[m_animation][m_frame]);

        GameObject::Update(m_game_scene);
    }

    void Puppet::Draw(sf::RenderWindow& p_window, sf::IntRect p_camera) {

        m_sprite.setPosition(transform().x() - p_camera.left, transform().y() - p_camera.top); // convert to local position
        p_window.draw(m_sprite);
    }
}
