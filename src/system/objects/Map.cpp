#include <iostream>

#include <SFML/Graphics.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "Salem2D/system/objects/Map.hpp"
#include "Salem2D/system/objects/MapLayer.hpp"
#include "Salem2D/system/objects/GameObject.hpp"
#include "Salem2D/system/objects/MapCapsule.hpp"

namespace Salem2D::Objects {
    Map::Map(const char* p_map_file) : m_map_capsule(std::make_unique<MapCapsule>()), m_mask_render_texture(new sf::RenderTexture), m_fringe_render_texture(new sf::RenderTexture)
    {
        try {
            std::ifstream json_file(p_map_file);
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(p_map_file, pt);

            // Load TileSheets
            for(boost::property_tree::ptree::value_type &child : pt.get_child("tilesets"))
            {
                auto first_gid = child.second.get<int>("firstgid");
                std::string tilesheet_path("data/maps/" + child.second.get<std::string>("source"));
                m_map_capsule->append_tile_sheet(
                        std::make_unique<Salem2D::Graphics::TileSheet>(tilesheet_path.data(), first_gid));
            }

            // Actual Map Settings
            m_tile_width = pt.get_child("tilewidth").get_value<unsigned int>();
            m_tile_height = pt.get_child("tileheight").get_value<unsigned int>();
            m_map_width = pt.get_child("width").get_value<unsigned int>();
            m_map_height = pt.get_child("height").get_value<unsigned int>();

            // Map Layers
            for (boost::property_tree::ptree::value_type &child : pt.get_child("layers")) {

                auto layer_name = child.second.get<std::string>("name");
                std::transform(layer_name.begin(), layer_name.end(), layer_name.begin(), ::tolower);

                auto visible = child.second.get<bool>("visible", true);
                auto opacity = child.second.get<float>("opacity");

                m_layers.push_back(std::make_unique<MapLayer>(layer_name, m_map_width, m_map_height, m_tile_width,
                                                              m_tile_height, opacity, visible));

                if(m_layers.back()->get_visible()) m_layers.back()->DrawTexture(m_map_capsule, as_vector<unsigned int>(child.second, "data"));
            }

            RenderBackground();
            RenderForeground();
            m_is_drawn = true;
        } catch(std::exception const &e)
        {
            std::cout << "Error loading map!" << std::endl;
            std::cerr << e.what() << std::endl;
        }
    }

    void Map::RenderBackground() {
        std::cout << "Generating mask texture..." << std::endl;
        m_mask_render_texture->create(m_map_width*m_tile_width, m_map_height*m_tile_height);
        std::cout << "Rendering Mask Texture" << std::endl;
        m_mask_render_texture->clear(sf::Color::Transparent);

        for(auto& map_layer : m_layers)
        {
            // Check if the layer name contains mask or sky
            if(map_layer->get_visible() && (map_layer->get_layer_name().find("bg") != std::string::npos
                                           || map_layer->get_layer_name().find("pg") != std::string::npos))
            {
                sf::Sprite sprite(map_layer->get_render_texture().getTexture());
                m_mask_render_texture->draw(sprite);

            }
        }
        m_mask_render_texture->display();
    }

    void Map::RenderForeground() {
        std::cout << "Generating fringe texture..." << std::endl;
        m_fringe_render_texture->create(m_map_width*m_tile_width, m_map_height*m_tile_height);
        std::cout << "Rendering Fringe Texture" << std::endl;
        m_fringe_render_texture->clear(sf::Color::Transparent);

        for(auto& map_layer : m_layers)
        {
            // Check if the layer name contains mask or sky
            if(map_layer->get_layer_name().find("fg") != std::string::npos)
            {
                sf::Sprite sprite(map_layer->get_render_texture().getTexture());
                m_fringe_render_texture->draw(sprite);
            }
        }

        m_fringe_render_texture->display();
    }

    void Map::DrawMask(sf::RenderWindow &p_window, sf::IntRect p_view) {
        sf::Texture const& texture = m_mask_render_texture->getTexture();
        sf::Sprite sprite(texture);
        sprite.setTextureRect(p_view);
        p_window.draw(sprite);
    }

    void Map::DrawFringe(sf::RenderWindow &p_window, sf::IntRect p_view) {
        sf::Sprite sprite(m_fringe_render_texture->getTexture());
        sprite.setTextureRect(p_view);
        p_window.draw(sprite);
    }
}
