#include "Salem2D/system/objects/MapLayer.hpp"
#include "Salem2D/system/objects/MapCapsule.hpp"
#include <iostream>
#include <memory>

namespace Salem2D::Objects {
    MapLayer::MapLayer(std::string p_layer_name,
                       unsigned int p_map_width, unsigned int p_map_height,
                       unsigned int p_tile_width, unsigned int p_tile_height,
                       float p_opacity, bool p_visible)
            :  m_tile_height(p_tile_height), m_tile_width(p_tile_width),
               m_map_width(p_map_width), m_map_height(p_map_height),
               m_opacity(p_opacity), m_visible(p_visible),
               m_layer_name(std::move(p_layer_name))
    {
        std::cout << "Generating Layer Texture..." << std::endl;
        m_render_texture.create(m_map_width*m_tile_width, m_map_height*m_tile_height);
        std::cout << "Drawing a map layer..." << std::endl;
    }

    void MapLayer::DrawTexture(std::unique_ptr<MapCapsule>& p_map_capsule, std::vector<unsigned int> p_data) {
        m_render_texture.clear(sf::Color::Transparent);
        int x = 0;
        int y = 0;
        for(unsigned int tile_val: p_data)
        {
            if(tile_val == 0) {
                ++x;
                if(x >= m_map_width){ x=0; ++y; }
                continue;
            }
            for(size_t i = 0; i < p_map_capsule->get_tile_sheet_count(); ++i)
            {
                auto tileSheet = p_map_capsule->get_tile_sheet_raw_pointer(i);

                if(tile_val >= tileSheet->get_first_gid() && tile_val <= (tileSheet->get_tile_count() + tileSheet->get_first_gid()))
                {
                    sf::Sprite sprite_tile(*tileSheet->GetTileTexture(tile_val));
                    sprite_tile.setPosition(x*m_tile_width, y*m_tile_height);
                    m_render_texture.draw(sprite_tile);
                    break;
                }
            }

            ++x;
            if(x >= m_map_width){ x=0; ++y; }
        }

        m_render_texture.display();
    }
}