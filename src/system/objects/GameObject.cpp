#include <vector>
#include <SFML/Graphics.hpp>

#include "Salem2D/system/objects/GameObject.hpp"

#include "Salem2D/physics/Transform.hpp"
#include "Salem2D/system/Behavior.hpp"


namespace Salem2D::Objects {
    GameObject::~GameObject() {
        for (auto& behavior : m_behaviors)
        {
            behavior->Destroy();
        }
    }

    void GameObject::Initialize() {
        for (auto& behavior : m_behaviors)
        {
            behavior->Initialize();
        }
    }

    void GameObject::LateUpdate() {
        for (auto& behavior : m_behaviors)
        {
            behavior->LateUpdate();
        }
    }

    void GameObject::Update(Salem2D::System::GameScene* m_game_scene) {
        for (auto& behavior : m_behaviors)
        {
            behavior->Update(m_game_scene, this);
        }
    }

    void GameObject::Update(Salem2D::Network::System::NetworkLoop* p_network_loop, Salem2D::Network::Objects::NetworkMap* p_map) {
        for (auto& behavior : m_behaviors)
        {
            behavior->Update(p_network_loop, p_map, this);
        }
    }

    void GameObject::FixedUpdate() {
        for (auto& behavior : m_behaviors)
        {
            behavior->FixedUpdate();
        }
    }

    void GameObject::Draw(sf::RenderWindow& p_window, sf::IntRect p_camera) {}

    void GameObject::AttachBehavior(std::unique_ptr<Salem2D::System::Behavior> p_behavior)
    {
        m_behaviors.push_back(std::move(p_behavior));
        m_behaviors.back()->OnBehaviorAttached();
    }

}