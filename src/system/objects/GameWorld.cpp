#include "Salem2D/system/objects/GameWorld.hpp"
#include <iostream>
#include <limits>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <SFML/Graphics.hpp>

#include "Salem2D/system/objects/Map.hpp"

namespace Salem2D::Objects {

    GameWorld::GameWorld(const char *p_world_file)
        : m_world_file(p_world_file)
    {
       /* try {
            std::ifstream json_file(p_world_file);
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(p_world_file, pt);

            unsigned int tile_width = pt.get_child("worldProperties").get_child("tileSize").get_child("width").get_value<unsigned int>();
            unsigned int tile_height = pt.get_child("worldProperties").get_child("tileSize").get_child("width").get_value<unsigned int>();

            // Find the lowest and highest X and Lowest Y for positions 0, 0 and the size of our world
            for(boost::property_tree::ptree::value_type &child : pt.get_child("maps"))
            {
                WorldTransform world_transform;
                world_transform.x = child.second.get<int>("x") * tile_width;
                world_transform.y = child.second.get<int>("y") * tile_height;
                world_transform.map_x = child.second.get<int>("map_x") * tile_width;
                world_transform.map_y = child.second.get<int>("map_y") * tile_height;
                world_transform.width = child.second.get<int>("width") * tile_width;
                world_transform.height = child.second.get<int>("height") * tile_height;

                m_map_matrix.insert(std::make_pair(world_transform, child.second.get<std::string>("file")));
            }
		}
		catch(Exception ) {

		}*/
    }

    // Determines which maps to load first
    void GameWorld::Initialize(sf::IntRect p_initial_view) {

        /*// Get the center of the player view
        int x = p_initial_view.left + (p_initial_view.width/2);
        int y = p_initial_view.top + (p_initial_view.height/2);

        // Find the map the player is on based on the center of the player's view
        const char* current_map = nullptr;
        WorldTransform& current_world_transform;
        for (auto it=m_map_matrix.begin(); it!=m_map_matrix.end(); ++it)
        {
            WorldTransform& world_transform = it->first;
            if(world_transform.x < x && world_transform.y < y;
                && world_transform.x + world_transform.width > x
                && world_transform.y + world_transform.height > y)
            {
                current_map = it->second.c_str();
                current_world_transform = it->first;
            }
        }

        // Find the maps touching this one
        const char* top_left = nullptr;
        const char* top_center = nullptr;
        const char* top_right = nullptr;
        const char* center_left = nullptr;
        const char* center = current_map;
        const char* center_right = nullptr;
        const char* bottom_left = nullptr;
        const char* bottom_center = nullptr;
        const char* bottom_right = nullptr;

        if(current_map != nullptr)
        {
            for (auto it=m_map_matrix.begin(); it!=m_map_matrix.end(); ++it)
            {
                WorldTransform& world_transform = it->first;

                // Top Left
                if(world_transform.map_x == current_world_transform.map_x -1 &&
                        world_transform.map_y == current_world_transform.map_y -1)
                {
                    top_left = it->second;
                }

                // Top Center
                if(world_transform.map_x == current_world_transform.map_x &&
                   world_transform.map_y == current_world_transform.map_y -1)
                {
                    top_center = it->second;
                }

                // Top Right
                if(world_transform.map_x == current_world_transform.map_x + 1 &&
                   world_transform.map_y == current_world_transform.map_y -1)
                {
                    top_right = it->second;
                }

                // Center Left
                if(world_transform.map_x == current_world_transform.map_x -1&&
                   world_transform.map_y == current_world_transform.map_y)
                {
                    center_left = it->second;
                }

                // Center Center is current map

                // Center Right
                if(world_transform.map_x == current_world_transform.map_x +1 &&
                   world_transform.map_y == current_world_transform.map_y)
                {
                    center_right = it->second;
                }

                // Bottom Left
                if(world_transform.map_x == current_world_transform.map_x -1 &&
                   world_transform.map_y == current_world_transform.map_y +1)
                {
                    bottom_left = it->second;
                }

                // Bottom Center
                if(world_transform.map_x == current_world_transform.map_x &&
                   world_transform.map_y == current_world_transform.map_y +1)
                {
                    bottom_center = it->second;
                }

                // Bottom Right
                if(world_transform.map_x == current_world_transform.map_x +1&&
                   world_transform.map_y == current_world_transform.map_y +1)
                {
                    bottom_right = it->second;
                }
            }
        }*/
    }

    void Update(sf::IntRect p_view) {
        // If the bounds of the view are no longer in the current loaded WorldTransform, shift the maps and render the new ones
    }

    void GameWorld::DrawMask(sf::RenderWindow &p_window, sf::IntRect p_view)
    {
        // convert the view to local map view
        //sf::IntRect local_view =

    }

    void GameWorld::DrawFringe(sf::RenderWindow &p_window, sf::IntRect p_view)
    {
        // conver the view to local map view
    }

}