#include <SFML/Graphics.hpp>

#include "Salem2D/system/GameScene.hpp"
#include "Salem2D/system/GameLoop.hpp"

#include <vector>
#include <memory>
namespace Salem2D::System {
    void GameLoop::Run()
    {
        while(m_window.isOpen())
        {
            HandleEvents();
            Update();
            m_window.clear();
            Draw(m_window);
            LateUpdate();
            m_window.display();
        }
    }

    void GameLoop::HandleEvents()
    {
        sf::Event event;
        while(m_window.pollEvent(event))
        {
            switch(event.type) {
                case sf::Event::Closed:
                    m_window.close();
                    break;

                    /* WINDOW EVENTS */
                case sf::Event::Resized:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnWindowResize(event);
                    break;
                case sf::Event::LostFocus:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnLostFocus(event);
                    break;
                case sf::Event::GainedFocus:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnGainedFocus(event);
                    break;
                case sf::Event::MouseEntered:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseEnteredWindow(event);
                    break;
                case sf::Event::MouseLeft:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseLeftWindow(event);
                    break;

                    /* INPUT EVENTS */
                case sf::Event::KeyPressed:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnKeyPressed(event);
                    break;
                case sf::Event::KeyReleased:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnKeyReleased(event);
                    break;
                case sf::Event::MouseWheelScrolled:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseScroll(event);
                    break;
                case sf::Event::MouseButtonPressed:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseDown(event);
                    break;
                case sf::Event::MouseButtonReleased:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseUp(event);
                    break;
                case sf::Event::MouseMoved:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnMouseMove(event);
                    break;
                case sf::Event::TextEntered:
                    if(m_scene_ptr.get() == nullptr) return;
                    m_scene_ptr->OnTextEntered(event);
                    break;

                    /* Joystick events available through SFML, but not yet implemented */
                default:
                    break;
            }
        }
    }

    void GameLoop::Draw(sf::RenderWindow& p_window)
    {
        if(m_scene_ptr == nullptr) return;
        m_scene_ptr->Draw(p_window);
    }

    void GameLoop::Update()
    {
        if(m_scene_ptr == nullptr) return;
        m_scene_ptr->Update();
    }

    void GameLoop::LateUpdate()
    {
        if(m_scene_ptr == nullptr) return;
        m_scene_ptr->LateUpdate();
    }

    void GameLoop::FixedUpdate()
    {
        if(m_scene_ptr == nullptr) return;
        m_scene_ptr->FixedUpdate();
    }
}