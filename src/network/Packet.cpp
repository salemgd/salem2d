#include <string>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network {
	Packet::Packet(std::string p_event_hook, std::unique_ptr<Events::NetworkObserver>& p_observer, size_t p_bytes_read)
            : m_event_hook_index(p_observer->get_hook_index(p_event_hook)), m_observer(p_observer), m_bytes_read(p_bytes_read)
    {}

    // TODO: Protect from buffer overloading
	Packet::Packet(const char* p_buf, std::unique_ptr<Events::NetworkObserver>& p_observer, size_t p_bytes_read)
	    : m_observer(p_observer), m_bytes_read(p_bytes_read)
    {
        size_t cursor = 0;
        cursor += sizeof(size_t); // Because we don't need to actually copy packet size

        // Copy Event Hook
        std::memcpy(&m_event_hook_index, p_buf + cursor, sizeof(m_event_hook_index));
        cursor += sizeof(m_event_hook_index);

        // Copy number of arguments
        size_t arg_count = 0;
        std::memcpy(&arg_count, p_buf + cursor, sizeof(arg_count));
        cursor += sizeof(arg_count);

        // Copy the arguments
        for(size_t i = 0; i < arg_count; ++i)
        {
            PacketArgument packet_argument;

            // Copy the size of the argument
            packet_argument.arg_size = 0;
            std::memcpy(&packet_argument.arg_size, p_buf + cursor, sizeof(size_t));
            cursor += sizeof(size_t);

            // Copy the argument
            packet_argument.arg_value = malloc(packet_argument.arg_size);
            std::memcpy(packet_argument.arg_value, p_buf + cursor, packet_argument.arg_size);
            cursor += packet_argument.arg_size;

            m_arguments.push_back(packet_argument);
        }
    }

    // TODO: Protect from buffer overloading
    const char* Packet::generate_buffer() {
        char* buf = reinterpret_cast<char*>(malloc(get_packet_size()));
        size_t cursor = 0;

        // Copy Packet Size
        size_t packet_size = get_packet_size();
        std::memcpy(buf + cursor, reinterpret_cast<void*>(&packet_size), sizeof(packet_size));
        cursor += sizeof(packet_size);

        // Copy Event Hook
        std::memcpy(buf + cursor, reinterpret_cast<void*>(&m_event_hook_index), sizeof(m_event_hook_index));
        cursor += sizeof(m_event_hook_index);

        // Copy Number of Arguments
        size_t arg_count = m_arguments.size();
        std::memcpy(buf + cursor, reinterpret_cast<char*>(&arg_count), sizeof(arg_count));
        cursor += sizeof(arg_count);

        // Copy the Arguments
        for(size_t i = 0; i < m_arguments.size(); ++i)
        {
            // Copy the Argument Size
            std::memcpy(buf + cursor, reinterpret_cast<char*>(&m_arguments[i].arg_size), sizeof(size_t));
            cursor += sizeof(size_t);

            // Copy the Argument
            std::memcpy(buf + cursor, m_arguments[i].arg_value, m_arguments[i].arg_size);
            cursor += m_arguments[i].arg_size;
        }

        return buf;
    }

    std::string Packet::get_event_hook()
    {
        return m_observer->get_hook(m_event_hook_index);
    }

    int Packet::get_event_hook_index() {
        return m_event_hook_index;
    }

    size_t Packet::get_packet_size() {
        size_t packet_size = 0;
        for(size_t i = 0; i < m_arguments.size(); ++i)
        {
            packet_size += m_arguments[i].arg_size; // Adding the size of the argument
            packet_size += sizeof(size_t); // Adding the size of the size of the argument
        }

        packet_size += sizeof(PacketHeader); // Because the packet has a header size
        return packet_size;
    }

	template<typename T> SALEM_API T Packet::GetArgument(size_t p_index)
	{
		return *reinterpret_cast<T*>(m_arguments[p_index].arg_value);
	}

	template<> SALEM_API const char* Packet::GetArgument<const char*>(size_t p_index)
    {
	    return reinterpret_cast<const char*>(m_arguments[p_index].arg_value);
    }

    template<> SALEM_API std::string Packet::GetArgument<std::string>(size_t p_index) {
	    return std::string(GetArgument<const char*>(p_index));
	}

	template SALEM_API int Packet::GetArgument<int>(size_t p_index);
	template SALEM_API char Packet::GetArgument<char>(size_t p_index);
	template SALEM_API long Packet::GetArgument<long>(size_t p_index);
	template SALEM_API long long Packet::GetArgument<long long>(size_t p_index);
	template SALEM_API float Packet::GetArgument<float>(size_t p_index);
	template SALEM_API double Packet::GetArgument<double>(size_t p_index);
	template SALEM_API unsigned int Packet::GetArgument<unsigned int>(size_t p_index);
	template SALEM_API unsigned long Packet::GetArgument<unsigned long>(size_t p_index);
	template SALEM_API unsigned long long Packet::GetArgument<unsigned long long>(size_t p_index);
	template SALEM_API bool Packet::GetArgument<bool>(size_t p_index);
}
