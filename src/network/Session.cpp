#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "Salem2D/network/Session.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/network/Server.hpp"
#include "Salem2D/network/Client.hpp"

namespace Salem2D::Network {
    Session::Session(boost::asio::io_service &p_io_service, std::unique_ptr<Events::NetworkObserver>& p_observer, Server* p_server)
        : m_socket(p_io_service), m_observer(p_observer), m_server(p_server), m_client(nullptr) {}

    Session::Session(boost::asio::io_service &p_io_service, std::unique_ptr<Events::NetworkObserver>& p_observer, Client* p_client)
            : m_socket(p_io_service), m_observer(p_observer), m_server(nullptr), m_client(p_client) {}

    void Session::Send(Packet& p_packet)
    {
        m_socket.async_send(boost::asio::buffer(p_packet.generate_buffer(), p_packet.get_packet_size()),
                            boost::bind(&Session::handle_packet_sent, this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
    }

    void Session::handle_packet_sent(const boost::system::error_code &p_error, size_t p_bytes_sent)
    {
        if(p_error) {
            std::cout << "Network Error (send): " << p_error.message() << std::endl;
            // TODO: Network Error Handling
        }
    }

    void Session::BeginRead() {
        m_socket.async_read_some(boost::asio::buffer(data, max_length),
                boost::bind(&Session::handle_packet_received, this,
                        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    }

    void Session::handle_packet_received(const boost::system::error_code &p_error, size_t p_bytes_read) {
        if (!p_error) {
            m_chunk_data.insert(m_chunk_data.end(), &data[0], &data[0] + p_bytes_read);

            while(m_chunk_data.size() >= sizeof(size_t)) {
                size_t packet_size = *reinterpret_cast<size_t *>(m_chunk_data.data());

                if (m_chunk_data.size() < packet_size) {
                    break;
                }

                Packet packet(m_chunk_data.data(), m_observer, packet_size);
                m_observer->Trigger(packet.get_event_hook(), packet, this);

                // Create a new vector consisting of all the information after p_bytes_read
                m_chunk_data = std::vector<char>(m_chunk_data.begin() + packet_size, m_chunk_data.end());
            }

            BeginRead();
        } else {
            // TODO: Network Error Handling
            std::cout << "Network Error (recv): " << p_error.message() << std::endl;
        }
    }
}
