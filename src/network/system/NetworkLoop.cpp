#include "Salem2D/network/system/NetworkLoop.hpp"
//#include <unistd.h>
namespace Salem2D::Network::System {
    void NetworkLoop::Run() {
        m_running = true;
        m_run_thread = std::thread(&NetworkLoop::RunThread, this);
    }

    void NetworkLoop::RunThread() {
        while(m_running) {
            Tick();
            //usleep(m_time_step);
        }
    }
}