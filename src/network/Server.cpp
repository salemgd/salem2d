
#include <boost/asio.hpp>
#include "Salem2D/network/Server.hpp"
#include "Salem2D/network/Session.hpp"
#include "Salem2D/network/system/NetworkLoop.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include <boost/bind.hpp>

namespace Salem2D::Network {
    Server::Server(unsigned short p_port, Salem2D::Network::System::NetworkLoop* p_network_loop)
            : m_io_service(), m_observer(new Events::NetworkObserver()), m_acceptor(m_io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), p_port)){
        auto netLoop = p_network_loop;
        netLoop->Run();
    }

    void Server::Run()
    {
        auto session = std::make_shared<Session>(m_io_service, m_observer, this);
        m_acceptor.async_accept(session->get_socket(),
                                boost::bind(&Server::handle_accept, this, session, boost::asio::placeholders::error));
        std::cout << "Network Status (listen): Listening for new connections" << std::endl;
        m_io_service.run();
    }

    void Server::handle_accept(std::shared_ptr<Session> p_session, const boost::system::error_code& p_error)
    {
        if(!p_error)
        {
            std::cout << "Network Status (accept): Connection accepted" << std::endl;
            p_session->set_index(m_session_index);
            ++m_session_index;

            m_sessions.push_back(p_session);
            p_session->BeginRead();

            std::shared_ptr<Session> session = std::make_shared<Session>(m_io_service, m_observer, this);

            m_acceptor.async_accept(session->get_socket(),
                                    boost::bind(&Server::handle_accept, this, session, boost::asio::placeholders::error));
        }
        else {
            std::cerr << "Network Error (accept): " << p_error.message() << std::endl;
            p_session.reset();
        }
    }

    std::shared_ptr<Session> Server::get_session(long long p_session_index)
    {
        for(size_t i = 0; i < m_sessions.size(); ++i)
        {
            if(m_sessions[i]->get_index() == p_session_index)
                return m_sessions[i];
        }

        return nullptr;
    }

    void Server::SendToAll(Packet& packet) {
        for(std::shared_ptr<Session>& session : m_sessions)
        {
            session->Send(packet);
        }
    }
}