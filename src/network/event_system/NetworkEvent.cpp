#include <string>
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network::Events {
    void NetworkEvent::Call(Packet& p_packet, Salem2D::Network::Session* p_session)
    {}
}