#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Packet.hpp"
#include <mutex>

namespace Salem2D::Network::Events {
    void NetworkObserver::Trigger(std::string p_hook, Packet& p_packet, Salem2D::Network::Session* p_session) {
		//std::lock_guard<std::mutex> lock(m_hook_map_mutex);
		for(size_t i = 0; i < m_subscribers.size(); ++i)
        {
		    if(m_subscribers[i] == nullptr)
		        continue;

            if(m_subscribers[i]->get_hook() == p_hook)
            {
                m_subscribers[i]->Call(p_packet, p_session);
            }
        }
    }

    int NetworkObserver::get_hook_index(std::string p_hook) {
		// std::lock_guard<std::mutex> lock(m_hook_map_mutex);
        auto it = m_hook_map.find(p_hook);
        if(it != m_hook_map.end())
        {
            return it->second;
        } else {
            return -1;
        }
    }

    std::string NetworkObserver::get_hook(int p_hook_index)
    {
		// std::lock_guard<std::mutex> lock(m_hook_map_mutex);
        auto it = m_reverse_hook_map.find(p_hook_index);
        if(it != m_reverse_hook_map.end())
        {
            return it->second;
        } else {
            return "undefined";
        }
    }

    int NetworkObserver::append_hook_to_map(std::string p_hook) {
        auto it = m_hook_map.find(p_hook);
        if(it == m_hook_map.end())
        {
            m_hook_map.insert(std::pair<std::string, int>(p_hook, m_hook_index));
            m_reverse_hook_map.insert(std::pair<int, std::string>(m_hook_index, p_hook));
            ++m_hook_index;

            return m_hook_index -1;
        } else {
            return it->second;
        }
    }

	void NetworkObserver::SubscribeEvent(std::string p_hook, NetworkEvent* p_network_event) {
		//std::lock_guard<std::mutex> lock(m_hook_map_mutex);
		append_hook_to_map(p_hook);
		p_network_event->set_hook(p_hook);
		m_subscribers.push_back(p_network_event);
	}
}
