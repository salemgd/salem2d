#include "Salem2D/network/Client.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"

namespace Salem2D::Network {
    Client::Client() : m_io_service(), m_observer(new Events::NetworkObserver()), m_session(m_io_service, m_observer, this)
	{
	}

    void Client::connect(const char* p_hostname, unsigned short p_port)
    {
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(p_hostname), p_port);
        m_session.get_socket().connect(endpoint);
        m_session.BeginRead();
        m_client_thread = std::thread(&Client::client_thread, this);
    }

    void Client::client_thread()
    {
        m_io_service.run();
    };
}