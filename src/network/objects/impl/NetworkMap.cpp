#include <iostream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "Salem2D/network/objects/impl/NetworkMap.hpp"
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/network/Session.hpp"

namespace Salem2D::Network::Objects {
    NetworkMap::NetworkMap(std::string p_map_file)
            : m_map_file(p_map_file), m_object_ptrs(), m_null_object_ptr(nullptr)
    {
        try {
            std::ifstream json_file(p_map_file);
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(p_map_file, pt);

            // Load the TileSheetData
            for(boost::property_tree::ptree::value_type& child : pt.get_child("tilesets"))
            {
                std::string tilesheet_path("data/maps/" + child.second.get<std::string>("source"));
                std::ifstream tilesheet_file(tilesheet_path.data());
                boost::property_tree::ptree tile_pt;
                boost::property_tree::read_json(tilesheet_file, tile_pt);

                // For each tile properties
                for(auto pair : tile_pt.get_child("tileproperties"))
                {
                    NetworkTile tile;
                    int key = std::stoi(pair.first);
                    int block_type = pair.second.get<int>("block-type");

                    tile.gid = key;
                    tile.blockType = block_type;

                    m_tiles.insert(std::pair<int, NetworkTile>(key, tile));
                }

                // Get Map properties
                m_tile_width = pt.get_child("tilewidth").get_value<unsigned int>();
                m_tile_height = pt.get_child("tileheight").get_value<unsigned int>();
                m_map_width = pt.get_child("width").get_value<unsigned int>();
                m_map_height = pt.get_child("height").get_value<unsigned int>();

                // Get map data for only the player layer
                for(auto &child : pt.get_child("layers"))
                {
                    auto layer_name = child.second.get<std::string>("name");
                    std::transform(layer_name.begin(), layer_name.end(), layer_name.begin(), ::tolower);

                    // is a playground level
                    if(layer_name.find("pg") != std::string::npos)
                    {
                        m_playground_data = as_vector<unsigned int>(child.second, "data");
                    }
                }
            }

        } catch (std::exception const &e)
        {
            std::cout << "Error loading map!" << std::endl;
            std::cerr << e.what() << std::endl;
        }
    }

    void NetworkMap::SendToMap(Salem2D::Network::Packet& packet)
    {
        for ( auto &[key, value] : m_object_ptrs ) {
            if(value->get_session() != nullptr)
            {
                value->get_session()->Send(packet);
            }
        }
    }

} // namespace Salem2D::Network::Objects