#include "Salem2D/graphics/ui/Gui.hpp"
#include "Salem2D/graphics/ui/controls/Button.hpp"

#include <iostream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace Salem2D::Graphics::Ui {
    Gui::Gui() : m_active_control(nullptr) {}

    Gui::Gui(const char *p_file) : m_controls(), m_active_control(nullptr), m_hover_control(nullptr) {
        if(p_file != nullptr)
        {
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(p_file, pt);

            // Load Fonts
            for(auto& child : pt.get_child("interface.fonts"))
            {
                auto font_id = child.second.get<std::string>("id");
                auto font_source = child.second.get<std::string>("source");

                m_fonts[font_id] = std::make_shared<sf::Font>();
                if(!FindFontById(font_id)->loadFromFile(font_source))
                {
                    std::cout << "[GUI] Error loading font: " << font_id << ": "<< font_source << std::endl;
                }
            }

            // Generate Interface Objects
            for(auto& child : pt.get_child("interface.objects")) {
                auto object_type = child.second.get<std::string>("type");
                auto object_id = child.second.get<std::string>("id");
                int z_index = child.second.get<int>("z-index", 0);

                std::shared_ptr<UiControl> control = CreateControl(object_type, object_id, child, z_index);
                if(control.get() != nullptr)
                {
                    AddControl(control, z_index);
                    std::cout << "control added" << std::endl;
                }
            }
        }
    }

    void Gui::Draw(sf::RenderWindow& p_window) {
        for(auto& [key, value] : m_controls)
        {
            value->Draw(p_window);
        }
    }

    std::shared_ptr<UiControl> Gui::CreateControl(std::string& p_object_type, std::string& p_object_id, boost::property_tree::ptree::value_type& p_control_data, int p_z_index)
    {
        if(p_object_type == "button")
        {
            return GenerateControl<Controls::Button>(p_object_id, p_control_data, p_z_index);
        }
        else if(p_object_type == "image")
        {

        }
        else if(p_object_type == "textbox")
        {

        }
        else if(p_object_id == "container")
        {

        }

        std::cout << "[Gui] Undefined UiControl Type" << std::endl;
        return std::shared_ptr<UiControl>(nullptr);
    }

    template<class T>
    std::shared_ptr<T> Gui::GenerateControl(std::string& p_object_id, boost::property_tree::ptree::value_type& p_control_data, int p_z_index)
    {
        ControlAttributes attributes = LoadAttributes(p_control_data);

        // Load Textures
        std::vector<std::string> texture_files;
        auto texture_node = p_control_data.second.get_child_optional("textures");

        if(texture_node)
        {
            for(auto& child : p_control_data.second.get_child("textures"))
            {
                std::string texture_path = child.second.get_value<std::string>();
                texture_files.push_back(texture_path);
            }
        }

        attributes.textures = texture_files;

        // Load BoxTransform
        double x = p_control_data.second.get<double>("position.x");
        double y = p_control_data.second.get<double>("position.y");
        double width = p_control_data.second.get<double>("size.width");
        double height = p_control_data.second.get<double>("size.height");

        sf::FloatRect box_transform(x, y, width, height);

        return std::make_shared<T>(attributes, box_transform, p_object_id);
    }

    ControlAttributes Gui::LoadAttributes(boost::property_tree::ptree::value_type& p_control_data)
    {
        auto attribute_node = p_control_data.second.get_child_optional("attributes");
        if(!attribute_node) return ControlAttributes();

        ControlAttributes attributes;

        std::string font_id = p_control_data.second.get<std::string>("attributes.font", "default");
        attributes.font = FindFontById(font_id);
        attributes.text_align = p_control_data.second.get<std::string>("attributes.text-align", "left");
        attributes.anchor = p_control_data.second.get<std::string>("attributes.anchor", "left");
        attributes.place_holder = p_control_data.second.get<std::string>("attributes.place-holder", "");
        std::string color_code = p_control_data.second.get<std::string>("attributes.font-color", "000000");
        //attributes.font_color = ConvertColorCodeToVector3(color_code);
        // TODO: Font Color Code
        attributes.font_size = p_control_data.second.get<unsigned int>("attributes.font-size", 12);
        attributes.is_password = p_control_data.second.get<bool>("attributes.is-password", false);
        attributes.label = p_control_data.second.get<std::string>("attributes.label", "");

        return attributes;
    }
}