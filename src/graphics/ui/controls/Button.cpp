#include "Salem2D/graphics/ui/controls/Button.hpp"

#include <iostream>

namespace Salem2D::Graphics::Ui::Controls {
    Button::Button(ControlAttributes p_attributes, sf::FloatRect p_box_transform, std::string p_object_id)
        : Salem2D::Graphics::Ui::UiControl(p_attributes, p_box_transform, std::move(p_object_id))
    {
        m_image.loadFromFile(p_attributes.textures[0]);

        m_texture_width = m_image.getSize().x/3;
        m_texture_height = m_image.getSize().y;

        for(unsigned int i = 0; i < 3; ++i)
        {
            sf::IntRect texture_rect(m_texture_width*i, 0, m_texture_width, m_texture_height);
            sf::Texture texture;
            texture.loadFromImage(m_image, texture_rect);
            m_state_textures.push_back(texture);
        }

        m_sprite.setTexture(m_state_textures[0]);

        m_label.setCharacterSize(p_attributes.font_size);
        m_label.setFont(*p_attributes.font);
        m_label.setString(p_attributes.label);
        m_label.setPosition(p_box_transform.left + p_box_transform.width/2 - m_label.getLocalBounds().width/2, p_box_transform.top + p_box_transform.height/2 - m_label.getLocalBounds().height/2);
    }

    void Button::Draw(sf::RenderWindow& p_window) {
        m_sprite.setPosition(Salem2D::Graphics::Ui::UiControl::box_transform().left, Salem2D::Graphics::Ui::UiControl::box_transform().top);
        m_sprite.setScale(box_transform().width/m_texture_width, box_transform().height/m_texture_height);
        p_window.draw(m_sprite);
        p_window.draw(m_label);
    }

    void Button::OnMouseEnter() {
        m_sprite.setTexture(m_state_textures[1]);
    }

    void Button::OnMouseLeave() {
        m_sprite.setTexture(m_state_textures[0]);
    }

    void Button::OnMouseDown(sf::Event event) {
        m_sprite.setTexture(m_state_textures[2]);
    }

    void Button::OnMouseUp(sf::Event event) {
        m_sprite.setTexture(m_state_textures[1]);

        if(on_click != nullptr) on_click();
    }
}