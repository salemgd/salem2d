#include <SFML/Graphics.hpp>
#include "Salem2D/graphics/TileSheet.hpp"

#include <sstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace Salem2D::Graphics {

        TileSheet::TileSheet(const char* p_tilesheet_path, int p_first_gid) : m_first_gid(p_first_gid), m_gid_on(p_first_gid)
        {
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(p_tilesheet_path, pt);

            // Tilesheet settings
            m_tile_width = pt.get_child("tilewidth").get_value<int>();
            m_tile_height = pt.get_child("tileheight").get_value<int>();
            m_tile_count = pt.get_child("tilecount").get_value<int>();
            std::string tileset_image = "data/maps/tilesheets/" + pt.get_child("image").get_value<std::string>();

            m_image.loadFromFile(tileset_image);

            m_tiles_x = m_image.getSize().x/m_tile_width;
            m_tiles_y = m_image.getSize().y/m_tile_width;

            int x = 0;
            int y = 0;
            for(int i = m_first_gid; i < m_first_gid + m_tile_count; ++i)
            {
                sf::IntRect texture_rect(m_tile_width*x, m_tile_height*y, m_tile_width, m_tile_height);

                Tile tile(std::shared_ptr<sf::Texture>(new sf::Texture),
                        static_cast<TileType>(pt.get<int>(std::string("tileproperties." + std::to_string(i) + ".block-type").c_str(), 0)));
                tile.get_texture()->loadFromImage(m_image, texture_rect);

                m_tile_sprite_matrix.push_back(tile);

                ++x;
                if(x >= m_tiles_x) { ++y; x=0; }
            }
        }
}
