#include "Player.hpp"
#include "Salem2D/network/Packet.hpp"

Salem2D::Network::Packet& Player::PushToPacket(Salem2D::Network::Packet& p_packet)
{
    auto& packet = Salem2D::Network::Objects::NetworkObject::PushToPacket(p_packet);
    packet.AddArgument(m_sprite);
    return packet;
}