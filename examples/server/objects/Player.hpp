#ifndef SALEM2D_PLAYER_HPP
#define SALEM2D_PLAYER_HPP

#include "Salem2D/network/objects/NetworkObject.hpp"
#include "Salem2D/network/Session.hpp"

namespace Salem2D::Network::System { class NetworkLoop; }
namespace Salem2D::Network { class Packet; }

class Player : public Salem2D::Network::Objects::NetworkObject {
public:
    Player(std::string p_object_id, float p_x, float p_y, float p_z, Salem2D::Network::Session* p_session, std::string& p_map)
            : Salem2D::Network::Objects::NetworkObject(std::move(p_object_id), p_x, p_y, p_z, p_session, 1), m_map(p_map)
        {}

        // getters
        SALEM_API std::string map() { return m_map; }
        SALEM_API std::string sprite() { return m_sprite; }

        // setters
        SALEM_API void set_map(const char* p_map) { m_map = p_map; }

        SALEM_API Salem2D::Network::Packet& PushToPacket(Salem2D::Network::Packet& p_packet) override;

private:
    std::string m_sprite = "data/gfx/sprites/sprite1.png";
    std::string m_map = "";
};

#endif //SALEM2D_PLAYER_HPP
