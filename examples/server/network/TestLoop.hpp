#ifndef SALEM2D_TESTLOOP_HPP
#define SALEM2D_TESTLOOP_HPP

#include "Salem2D/network/system/NetworkLoop.hpp"
#include "Salem2D/network/objects/impl/NetworkMap.hpp"

#include <map>
#include "../objects/Player.hpp"

class TestLoop : public Salem2D::Network::System::NetworkLoop {
public:
    TestLoop() : Salem2D::Network::System::NetworkLoop(10) {}

    void Initialize() override {

        NetworkLoop::Initialize();
    }

    void Tick() override {
        NetworkLoop::Tick();
    }

    void InsertIndexedPlayer(long long p_session_id, std::shared_ptr<Player> p_player) {
        m_players.insert(std::make_pair(p_session_id, p_player));
    }

    std::shared_ptr<Player> GetIndexedPlayer(long long p_session_id)
    {
        if(m_players.find(p_session_id) != m_players.end())
        {
            return m_players[p_session_id];
        }

        return std::shared_ptr<Player>(nullptr);
    }

private:
    std::map<long long, std::shared_ptr<Player>> m_players;
};

#endif //SALEM2D_TESTLOOP_HPP
