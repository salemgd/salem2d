#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/Packet.hpp"

#ifndef SALEM2D_CHANGEMAPEVENT_HPP
#define SALEM2D_CHANGEMAPEVENT_HPP

class ChangeMapEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    ChangeMapEvent() : Salem2D::Network::Events::NetworkEvent() {}

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session*) override {}
};


#endif //SALEM2D_CHANGEMAPEVENT_HPP
