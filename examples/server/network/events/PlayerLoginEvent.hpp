#ifndef SALEM2D_PLAYERLOGINEVENT_HPP
#define SALEM2D_PLAYERLOGINEVENT_HPP

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Session.hpp"
#include "Salem2D/network/Server.hpp"
#include "Salem2D/network/Packet.hpp"
#include "../TestLoop.hpp"

#include "../../objects/Player.hpp"

class PlayerLoginEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    explicit PlayerLoginEvent(TestLoop* p_loop)
            : Salem2D::Network::Events::NetworkEvent(){
        m_loop = p_loop;
    }

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session* p_session) override {
        std::cout << "hit player login call" << std::endl;

        //const char* username = p_packet->GetArgument<const char*>(0);
        //const char* password = p_packet->GetArgument<const char*>(1);

        // Get Player Map
        std::string map_path("data/maps/temple.json");
        auto& map = m_loop->get_map(map_path);

        if(map.get() == nullptr)
        {
            std::cout << "Could not find loaded map: " << map_path << std::endl;
            return;
        }

        // Spawn Player
        auto object_id = std::string("player_") + std::to_string(m_index);
        auto player = map->SpawnNetworkObject<Player>(object_id, new Player(object_id, 32, 32, 0, p_session, map_path));

        // Add player to networkLoop Connections
        m_loop->InsertIndexedPlayer(p_session->get_index(), player);

        // Send Load Map
        Salem2D::Network::Packet load_map_packet("ChangeMapEvent", p_session->get_observer());
        load_map_packet.AddArgument(player->map());
        p_session->Send(load_map_packet);
        std::cout << "Send Load Map" << std::endl;

        // SendPlayerObjectToMap
        Salem2D::Network::Packet spawn_packet("ObjectSpawnEvent", p_session->get_observer());
        player->PushToPacket(spawn_packet);
        map->SendToMap(spawn_packet);
        std::cout << "Send spawn packet" << std::endl;

        // Send Local Entities
        auto& object_ptrs = map->object_ptrs();
        for(auto &[key, value] : object_ptrs)
        {
            Salem2D::Network::Packet packet("ObjectSpawnEvent", p_session->get_observer());
            value->PushToPacket(packet);
            p_session->Send(packet);
        }

        std::cout << "hit end of login call" << std::endl;
        m_index++;
    }

private:
    TestLoop* m_loop;
    int m_index = 0;
};

#endif //SALEM2D_PLAYERLOGINEVENT_HPP
