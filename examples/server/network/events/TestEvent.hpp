#ifndef SALEM2D_TESTEVENT_HPP
#define SALEM2D_TESTEVENT_HPP

#include <iostream>
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Packet.hpp"

class TestEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    TestEvent()
        : Salem2D::Network::Events::NetworkEvent(){}

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session* p_session) override {
        std::cout << "Packet Received" << std::endl;
        auto message = p_packet.GetArgument<const char*>(0);
        std::cout << "Network Message: " << message << std::endl;
    }
};

#endif //SALEM2D_TESTEVENT_HPP
