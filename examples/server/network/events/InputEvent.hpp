#ifndef SALEM2D_INPUTEVENT_HPP
#define SALEM2D_INPUTEVENT_HPP

#include <iostream>
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Packet.hpp"

#include "../TestLoop.hpp"

class InputEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    InputEvent(TestLoop* p_loop)
            : Salem2D::Network::Events::NetworkEvent(), m_loop(p_loop){}

    void Call(Salem2D::Network::Packet &p_packet, Salem2D::Network::Session* p_session) override {
        // Get player by SessionId
        auto player = m_loop->GetIndexedPlayer(p_session->get_index());
        if(player.get() == nullptr)
        {
            std::cout << "Could not find player by index: " + std::to_string(p_session->get_index()) << std::endl;
            return;
        }

        char direction = *p_packet.GetArgument<const char*>(0);

        float x = player->transform().x();
        float y = player->transform().y();
        float z = player->transform().z();

        switch(direction){
            case 'W':{
                player->transform().set_y(y -3);
                break;
            }
            case 'A':{
                player->transform().set_x(x -3);
                break;
            }
            case 'S':{
                player->transform().set_y(y +3);
                break;
            }
            case 'D':{
                player->transform().set_x(x +3);
                break;
            }
            default:
                std::cout << "Invalid Direction" << std::endl;
        }

        auto& map = m_loop->get_map(player->map());

        if(map.get() != nullptr)
        {
            Salem2D::Network::Packet update_packet(std::string("InputEvent"), p_session->get_observer());
            player->PushToPacket(update_packet);
            map->SendToMap(update_packet);
        }
    }

private:
    TestLoop* m_loop;
};

#endif //SALEM2D_INPUTEVENT_HPP
