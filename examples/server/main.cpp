#include <iostream>
#include "Salem2D/network/Server.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "network/events/TestEvent.hpp"
#include "network/events/InputEvent.hpp"
#include "network/events/PlayerLoginEvent.hpp"
#include "network/events/ChangeMapEvent.hpp"
#include "network/TestLoop.hpp"

int main()
{
    TestLoop* loop = new TestLoop();
    Salem2D::Network::Server server(3000, loop);

    server.get_observer()->SubscribeEvent("TestEvent", new TestEvent());
    server.get_observer()->SubscribeEvent("InputEvent", new InputEvent(loop));
    server.get_observer()->SubscribeEvent("PlayerLoginEvent", new PlayerLoginEvent(loop));
    server.get_observer()->SubscribeEvent("ChangeMapEvent", new ChangeMapEvent());
    server.get_observer()->SubscribeEvent("ObjectSpawnEvent", new TestEvent());

    server.Run();

    return 0;
}