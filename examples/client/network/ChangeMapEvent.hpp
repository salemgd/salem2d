#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/Packet.hpp"

#ifndef SALEM2D_CHANGEMAPEVENT_HPP
#define SALEM2D_CHANGEMAPEVENT_HPP

class ChangeMapEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    ChangeMapEvent(Salem2D::System::GameScene* p_scene) : Salem2D::Network::Events::NetworkEvent()
    {
        m_scene = p_scene;
    }

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session*) override {
        auto map_file = p_packet.GetArgument<const char*>(0);
        m_scene->LoadMap(map_file);
    }

private:
    Salem2D::System::GameScene* m_scene;
};


#endif //SALEM2D_CHANGEMAPEVENT_HPP
