#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/Packet.hpp"

#ifndef SALEM2D_PLAYERUPDATEEVENT_HPP
#define SALEM2D_PLAYERUPDATEEVENT_HPP

class PlayerUpdateEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    PlayerUpdateEvent(TestScene* p_scene)
            : Salem2D::Network::Events::NetworkEvent(), m_scene(p_scene) {}

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session*) override {
        auto object_id = p_packet.GetArgument<const char*>(1);
        auto& object = m_scene->FindGameObject(object_id);

        if(object.get() == nullptr)
        {
            std::cout << "Could not find object by the id: " << object_id << std::endl;
            return;
        }

        auto x = p_packet.GetArgument<float>(2);
        auto y = p_packet.GetArgument<float>(3);
        auto z = p_packet.GetArgument<float>(4);

        object->transform().set_x(x);
        object->transform().set_y(y);
        object->transform().set_z(z);
    }
private:
    TestScene* m_scene;
};


#endif //SALEM2D_PLAYERUPDATEEVENT_HPP
