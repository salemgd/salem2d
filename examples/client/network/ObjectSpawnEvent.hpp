#ifndef SALEM2D_OBJECTSPAWNEVENT_HPP
#define SALEM2D_OBJECTSPAWNEVENT_HPP

#include <iostream>
#include "Salem2D/network/event_system/NetworkEvent.hpp"
#include "Salem2D/network/event_system/NetworkObserver.hpp"
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/system/GameScene.hpp"
#include "Salem2D/system/objects/impl/Puppet.hpp"

class ObjectSpawnEvent : public Salem2D::Network::Events::NetworkEvent {
public:
    ObjectSpawnEvent(Salem2D::System::GameScene* p_scene)
        : Salem2D::Network::Events::NetworkEvent(), m_game_scene(p_scene)
    {

    }

    void Call(Salem2D::Network::Packet& p_packet, Salem2D::Network::Session*) override {
        std::cout << "Spawning Network Object..." << std::endl;

        unsigned int objectType = p_packet.GetArgument<unsigned int>(0);
        auto object_id = p_packet.GetArgument<std::string>(1);

        switch(objectType)
        {
            case 1: {

                if(m_game_scene->FindGameObject(object_id).get() != nullptr)
                {
                    std::cout << "Received a spawn game object packet, but object, " << object_id << ", already exists" << std::endl;
                    break;
                }

                auto x = p_packet.GetArgument<float>(2);
                auto y = p_packet.GetArgument<float>(3);
                auto z = p_packet.GetArgument<float>(4);
                auto spritesheet = p_packet.GetArgument<const char*>(5);

                m_game_scene->SpawnGameObject(object_id, std::unique_ptr<Salem2D::Objects::Impl::Puppet>(
                        new Salem2D::Objects::Impl::Puppet(object_id, x, y, z, spritesheet, 32, 64)));

                std::cout << "Spawned Puppet" << std::endl;
                break;
            }
            default:
                std::cout << "Received an unknown objectType..." << std::endl;
                break;
        }
    }
private:
    Salem2D::System::GameScene* m_game_scene;
};

#endif //SALEM2D_OBJECTSPAWNEVENT_HPP
