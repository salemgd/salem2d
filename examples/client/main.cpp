#include "Salem2D/system/GameLoop.hpp"
#include "Salem2D/network/Client.hpp"
#include "scenes/TestScene.hpp"
#include "network/TestEvent.hpp"
#include "network/PlayerUpdateEvent.hpp"
#include "network/ChangeMapEvent.hpp"
#include "network/ObjectSpawnEvent.hpp"

int main()
{
    Salem2D::Network::Client client;
    Salem2D::System::GameLoop gameLoop(new TestScene(&client));

    client.get_observer()->SubscribeEvent("TestEvent",  new TestEvent());
    client.get_observer()->SubscribeEvent("InputEvent",  new PlayerUpdateEvent(reinterpret_cast<TestScene*>(gameLoop.scene_ptr().get())));
    client.get_observer()->SubscribeEvent("PlayerLoginEvent",  new TestEvent());
    client.get_observer()->SubscribeEvent("ChangeMapEvent", new ChangeMapEvent(gameLoop.scene_ptr().get()));
    client.get_observer()->SubscribeEvent("ObjectSpawnEvent",  new ObjectSpawnEvent(gameLoop.scene_ptr().get()));
    client.connect("127.0.0.1", 3000);

    gameLoop.Run();

    return 0;
}