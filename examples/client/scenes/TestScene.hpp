#ifndef SALEM2D_TESTSCENE_HPP
#define SALEM2D_TESTSCENE_HPP

#include <iostream>
#include "Salem2D/system/GameScene.hpp"
#include "Salem2D/system/objects/impl/Puppet.hpp"
#include "../behaviors/PlayerController.hpp"
#include "Salem2D/network/Client.hpp"
#include "Salem2D/network/Packet.hpp"
#include "Salem2D/graphics/ui/controls/Button.hpp"

class TestScene : public Salem2D::System::GameScene {
public:
    TestScene(Salem2D::Network::Client* p_client) : Salem2D::System::GameScene(), m_client(p_client) {
        m_controller = SpawnGameObject("controller", std::make_unique<Salem2D::Objects::GameObject>(1480.0f, 2989.0f, 1.0f, "controller")).get();
    }

    void Initialize() override {

        m_controller->AttachBehavior(std::make_unique<PlayerController>(m_client));
        GameScene::Initialize();
        LoadGui("data/gfx/gui/views/MainMenu.json");
        gui().FindControlById<Salem2D::Graphics::Ui::Controls::Button>("btn_login")->on_click = [](){
            std::cout << "Button Clicked" << std::endl;
        };
    }

    void Update() override {
        if(!first_load)
        {
            Salem2D::Network::Packet packet(std::string("PlayerLoginEvent"), m_client->get_observer());
            m_client->get_session().Send(packet);
            first_load = true;
        }

//        int x = int(m_player->transform().x()) - (main_window()->getSize().x/2);
////        int y = int(m_player->transform().y()) - (get_main_window()->getSize().y/2);
////
////        if(get_map_ptr() != nullptr) {
////            x = x + get_main_window()->getSize().x > get_map_ptr()->get_width() ? get_map_ptr()->get_width() -
////                                                                                  get_main_window()->getSize().x : x;
////            y = y + get_main_window()->getSize().y > get_map_ptr()->get_height() ? get_map_ptr()->get_height() -
////                                                                                   get_main_window()->getSize().y : y;
////            x = x < 0 ? 0 : x;
////            y = y < 0 ? 0 : y;
////        }
////
////        set_camera(sf::IntRect(x, y, get_main_window()->getSize().x, get_main_window()->getSize().y));
////
        set_camera(sf::IntRect(0, 0, main_window()->getSize().x, main_window()->getSize().y));
        GameScene::Update();
    }

private:
    Salem2D::Objects::GameObject* m_controller;
    Salem2D::Network::Client* m_client;
    bool first_load = false;
};

#endif //SALEM2D_TESTSCENE_HPP
