#ifndef SALEM2D_PLAYERCONTROLLER_HPP
#define SALEM2D_PLAYERCONTROLLER_HPP

#include <SFML/Window.hpp>
#include <Salem2D/system/GameScene.hpp>
#include <Salem2D/system/Behavior.hpp>
#include <Salem2D/system/objects/impl/Puppet.hpp>

#include "Salem2D/network/Client.hpp"
#include "Salem2D/network/Packet.hpp"

class PlayerController : public Salem2D::System::Behavior {
public:
    PlayerController(Salem2D::Network::Client* p_client) {
        m_client = p_client;
    }

    void Update(Salem2D::System::GameScene* m_game_scene, Salem2D::Objects::GameObject* p_game_object) override
    {
        auto puppet = reinterpret_cast<Salem2D::Objects::Impl::Puppet*>(p_game_object);

        if(!m_game_scene->main_window()->hasFocus())
            return;

        /* SERVER CONTROLS */
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            SendDirection('D');
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            SendDirection('A');
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            SendDirection('S');
        }
        else if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            SendDirection('W');
        }
    }
private:
    Salem2D::Network::Client* m_client;

    void SendDirection(char p_direction)
    {
        Salem2D::Network::Packet packet(std::string("InputEvent"), m_client->get_observer());
        packet.AddArgument(sizeof(char), &p_direction);
        m_client->get_session().Send(packet);
    }
};

#endif //SALEM2D_PLAYERCONTROLLER_HPP
