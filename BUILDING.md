# Building Salem GDK

## Supported Platforms/Configurations
The following configurations have been tested to work with Salem2D, but others may too:

- [Linux](#preparing-your-linux-environment)
  - [Arch Linux](#preparing-arch-linux)
    - `Manjaro 17.1.6`
- [MacOS/OSX](#preparing-your-macos/osx-environment)
  - `MacOS High Sierra`
- [Windows](#building-salem2d-in-visual-studio)
  - `10.0.16299`

## Building Salem2D in Visual Studio
### Dependencies
 - `Boost` `1.66.x+` (Windows Only: compiled and included for Vs2017 only)
 - `SFML` `2.5.x+` (Windows Only: compiled and built for Vs2017 only)

### building
 - Navigate to the root directory of the Salem2D Source and open salem2d.sln
 - The project is setup with its dependencies built and installed within the "thirdparty" folder
 - You should just be able to build and run without any extra required steps
 - You will need to build Salem2D (the library) before building the test server/client
  - Right Click on the Salem2D Project and click "build" (will output to <root_dir>/bin/lib/)
  - After build the client and server. The build process will copy the resources and the libraries for you.
 - Building the whole solution will build all 3 in their necessary order.
 - Running the whole solution will build and run the files in the following order:
  - [Build] Salem2d.lib/.dll -> [Build] [Run] Test Server -> [Build] [Run] Test Client

### Setup and Compile


## Preparing your Linux Environment
### Arch Linux
 - Install `sfml` from pacman using `sudo pacman -S sfml`
 - Install `boost` from pacman using `sudp pacman -S boost`
 - Install `lib32-glibc` version `2.27` from pacman using `sudo pacman -S lib32-glibc`

## Building SalemGDK with Mac OsX and CLion
### Prerequisites
 - If you haven't already, install [`brew`](https://brew.sh/) available [here](https://brew.sh/).
 - If you haven't already, install `cmake`, available with `brew install cmake`.

### Installing dependencies
 - Install `boost` `1.66.0` or higher with `sudo brew install boost`.
 - Install `SFML` `1.4.2` or higher with `sudo brew install sfml`.

### Notes
- To check for dependencies, run `brew search <name>` or check [here](http://formulae.brew.sh/)
- To check versions, either run `brew info <name>` or check [here](http://formulae.brew.sh/)
